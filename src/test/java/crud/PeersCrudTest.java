package crud;

import nl.aimsites.nzbvuq.datastorage.crud.PeersCrudImpl;
import nl.aimsites.nzbvuq.datastorage.dao.PeerDao;
import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class PeersCrudTest {
    private PeersCrudImpl peersCrud;
    private PeerDao mockedPeerDao;
    private PeerDto examplePeerDto;

    @BeforeEach
    void setup() {
        peersCrud = new PeersCrudImpl();

        mockedPeerDao = Mockito.mock(PeerDao.class);
        peersCrud.setPeerDao(mockedPeerDao);

        examplePeerDto = new PeerDto();
        examplePeerDto.setId("id");
    }

    @Test
    void testCreate() {
        // Arrange

        // Act
        peersCrud.create(examplePeerDto);

        // Assert
        Mockito.verify(mockedPeerDao).create(examplePeerDto);
    }

    @Test
    void testRead() {
        // Arrange
        List<PeerDto> peerDtos = new ArrayList<>();
        peerDtos.add(examplePeerDto);
        Mockito.when(mockedPeerDao.read()).thenReturn(peerDtos);

        // Act
        List<PeerDto> response = peersCrud.read();

        // Assert
        Assertions.assertEquals(peerDtos, response);
    }

    @Test
    void testUpdate() {
        // Arrange

        // Act
        peersCrud.update(examplePeerDto, examplePeerDto);

        // Assert
        Mockito.verify(mockedPeerDao).update(examplePeerDto, examplePeerDto);
    }

    @Test
    void testDelete() {
        // Arrange

        // Act
        peersCrud.delete(examplePeerDto);

        // Assert
        Mockito.verify(mockedPeerDao).delete(examplePeerDto);
    }
}
