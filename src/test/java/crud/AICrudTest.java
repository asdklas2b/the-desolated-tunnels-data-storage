package crud;

import nl.aimsites.nzbvuq.datastorage.crud.AICrudImpl;
import nl.aimsites.nzbvuq.datastorage.dao.AIDao;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AgentDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

public class AICrudTest {
    private AICrudImpl aiCrud;
    private AIDao mockedAiDao;
    private AIDto exampleAiDto;

    @BeforeEach
    void setup() {
        aiCrud = new AICrudImpl();

        mockedAiDao = Mockito.mock(AIDao.class);
        aiCrud.setAiDao(mockedAiDao);

        exampleAiDto = new AgentDto();
    }

    @Test
    void testCreate() {
        // Arrange

        // Act
        aiCrud.create(exampleAiDto);

        // Assert
        Mockito.verify(mockedAiDao).create(exampleAiDto);
    }

    @Test
    void testRead() {
        // Arrange
        List<AIDto> exampleAiDtoList = new ArrayList<>();
        exampleAiDtoList.add(exampleAiDto);
        Mockito.when(mockedAiDao.read()).thenReturn(exampleAiDtoList);

        // Act
        List<AIDto> response = aiCrud.read();

        // Assert
        Assertions.assertEquals(exampleAiDtoList, response);
    }

    @Test
    void testUpdate() {
        // Arrange

        // Act
        aiCrud.update(exampleAiDto);

        // Assert
        Mockito.verify(mockedAiDao).delete(exampleAiDto);
        Mockito.verify(mockedAiDao).create(exampleAiDto);
    }

    @Test
    void testDelete() {
        // Arrange

        // Act
        aiCrud.delete(exampleAiDto);

        // Assert
        Mockito.verify(mockedAiDao).delete(exampleAiDto);
    }
}
