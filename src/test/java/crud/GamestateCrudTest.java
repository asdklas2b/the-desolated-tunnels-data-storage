package crud;

import nl.aimsites.nzbvuq.datastorage.crud.GamestateCrudImpl;
import nl.aimsites.nzbvuq.datastorage.dao.impl.GraphDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.GamestateDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.GraphDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.List;

public class GamestateCrudTest {
  private GamestateCrudImpl gamestateCrud;
  private GraphDaoImpl worldAreaDao;
  private List<GraphDto> graphDtos;
  private GamestateDto gamestateDto;

  @BeforeEach
  void setup() {
    gamestateCrud = new GamestateCrudImpl();
    worldAreaDao = Mockito.mock(GraphDaoImpl.class);
    gamestateCrud.setGraphDao(worldAreaDao);
    graphDtos = new ArrayList<>();
    graphDtos.add(new GraphDto(1, 2, 4, 4));
    gamestateDto = new GamestateDto();
    gamestateDto.setGraphs(graphDtos);
  }

  @Test
  void testRead() {
    // Arrange
    Mockito.when(worldAreaDao.read()).thenReturn(graphDtos);

    // Act
    GamestateDto response = gamestateCrud.read();

    // Assert
    Assertions.assertEquals(graphDtos, response.getGraphs());
  }

  @Test
  void testCreate() {
    // Arrange

    // Act
    gamestateCrud.create(gamestateDto);

    // Assert
    Mockito.verify(worldAreaDao).create(Mockito.any());
  }

  @Test
  void testUpdate() {
    // Arrange

    // Act
    gamestateCrud.update(gamestateDto);

    // Assert
    Mockito.verify(worldAreaDao).delete();
    Mockito.verify(worldAreaDao).create(Mockito.any());
  }

  @Test
  void testDelete() {
    // Arrange
    GamestateDto gamestateDto = new GamestateDto();
    List<GraphDto> graphDtos = new ArrayList<>();
    graphDtos.add(new GraphDto());

    // Act
    gamestateCrud.delete();

    // Assert
    Mockito.verify(worldAreaDao).delete();
  }
}
