package database;

import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.database.QueryLoader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DatabaseStatementTest {
  private Connection connection;
  private DatabaseStatement databaseStatement;
  private PreparedStatement preparedStatement;
  private QueryLoader loader;
  private String query;

  @BeforeEach
  void setup() throws SQLException {
    connection = Mockito.mock(Connection.class);
    preparedStatement = Mockito.mock(PreparedStatement.class);
    query = "TEST";
    String table = "";
    Mockito.when(connection.prepareStatement(Mockito.anyString())).thenReturn(preparedStatement);

    // Loader
    loader = Mockito.mock(QueryLoader.class);
    Mockito.when(loader.loadQueryString(table, query)).thenReturn("");

    databaseStatement = new DatabaseStatement(connection, table, query, loader);
  }

  @Test
  void testSetInt() throws SQLException {
    // Arrange
    int index = 0;
    int value = 1;

    // Act
    databaseStatement.setInt(index, value);

    // Assert
    Mockito.verify(preparedStatement).setInt(index, value);
  }

  @Test
  void testSetString() throws SQLException {
    // Arrange
    int index = 0;
    String value = "test";

    // Act
    databaseStatement.setString(index, value);

    // Assert
    Mockito.verify(preparedStatement).setString(index, value);
  }

  @Test
  void testExecute() throws SQLException {
    // Act
    databaseStatement.execute();

    // Assert
    Mockito.verify(preparedStatement).execute();
  }

  @Test
  void executeQuery() throws SQLException {
    // Arrange
    ResultSet resultSet = Mockito.mock(ResultSet.class);
    Mockito.when(databaseStatement.executeQuery()).thenReturn(resultSet);

    // Act
    ResultSet response = databaseStatement.executeQuery();

    // Assert
    assertEquals(resultSet, response);
  }

  @Test
  void testConstruct() {
    // Arrange
    Connection connection = Mockito.mock(Connection.class);

    // Act
    DatabaseStatement databaseStatement = new DatabaseStatement(connection, "ai", "select");

    // Assert
    Assertions.assertNotNull(databaseStatement);
  }

  @Test
  void testSetTableName() throws SQLException {
    // Arrange

    // Act
    databaseStatement.setTableName("tabel");

    // Assert
    Mockito.verify(connection, Mockito.atLeast(1)).prepareStatement(Mockito.anyString());
  }

  @Test
  void testSetLong() throws SQLException {
    // Arrange
    int index = 0;
    long value = 5;

    // Act
    databaseStatement.setLong(index, value);

    // Assert
    Mockito.verify(preparedStatement).setLong(index, value);
  }
}
