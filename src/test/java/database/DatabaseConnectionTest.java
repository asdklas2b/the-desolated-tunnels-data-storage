package database;

import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.Connection;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DatabaseConnectionTest {
  private DatabaseConnection databaseConnection;
  private Connection connection;

  @BeforeEach
  void setup() {
    databaseConnection = new DatabaseConnection("ai");
    connection = Mockito.mock(Connection.class);
    databaseConnection.setConnection(connection);
  }

  @Test
  void testGetSetConnection() {
    // Arrange

    // Act
    Connection response = databaseConnection.getConnection();

    // Assert
    assertEquals(connection, response);
  }

  @Test
  void testCreateStatement() {
    // Arrange

    // Act
    DatabaseStatement statement = databaseConnection.createStatement("select");

    // Assert
    Assertions.assertNotNull(statement);
  }

  @Test
  void testConnect() {
    // Arrange

    // Act
    databaseConnection.connect();

    // Assert
    Assertions.assertNotNull(databaseConnection.getConnection());
  }

  @Test
  void testClose() {
    // Arrange

    // Act
    databaseConnection.close();

    // Assert
    Assertions.assertNotNull(databaseConnection.getConnection());
  }
}
