package database;

import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Properties;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class DatabasePropertiesTest {
  private DatabaseProperties databaseProperties;
  private String expected;

  @BeforeEach
  void setup() {
    databaseProperties = new DatabaseProperties();
    Properties properties = Mockito.mock(Properties.class);
    databaseProperties.setProperties(properties);

    expected = "test";
    Mockito.when(properties.getProperty(Mockito.anyString())).thenReturn(expected);
  }

  @Test
  void testGetDriver() {
    // Act
    String response = databaseProperties.getDriver();

    // Assert
    assertEquals(expected, response);
  }

  @Test
  void testGetConnectionString() {
    // Act
    String response = databaseProperties.getDriver();

    // Assert
    assertEquals(expected, response);
  }
}
