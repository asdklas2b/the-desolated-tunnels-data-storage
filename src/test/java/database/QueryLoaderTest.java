package database;

import nl.aimsites.nzbvuq.datastorage.dao.database.QueryLoader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class QueryLoaderTest {
  private QueryLoader queryLoader;
  private Properties mockedProperties;

  @BeforeEach
  void setup() {
    queryLoader = new QueryLoader();

    mockedProperties = Mockito.mock(Properties.class);
    Mockito.when(mockedProperties.getProperty(Mockito.anyString())).thenReturn("test");
    queryLoader.setProperties(mockedProperties);
  }

  @Test
  void testLoad() throws IOException {
    // Arrange

    // Act
    String response = queryLoader.loadQueryString("test", "test");

    // Assert
    Mockito.verify(mockedProperties).load((InputStream) Mockito.any());
    Assertions.assertEquals(response, "test");
  }
}
