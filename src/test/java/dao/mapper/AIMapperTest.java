package dao.mapper;

import nl.aimsites.nzbvuq.datastorage.dao.mapper.AIMapper;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AgentDto;
import nl.aimsites.nzbvuq.datastorage.dto.ai.MonsterDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class AIMapperTest {
    @Test
    void testMapAgent() {
        // Arrange

        // Act
        AIDto aiDto = AIMapper.map("Agent");

        // Assert
        Assertions.assertTrue(aiDto instanceof AgentDto);
    }

    @Test
    void testMapMonster() {
        // Arrange

        // Act
        AIDto aiDto = AIMapper.map("Monster");

        // Assert
        Assertions.assertTrue(aiDto instanceof MonsterDto);
    }

    @Test
    void testMapNull() {
        // Arrange

        // Act
        AIDto aiDto = AIMapper.map("niks");

        // Assert
        Assertions.assertNull(aiDto);
    }
}
