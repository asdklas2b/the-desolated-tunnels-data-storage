package dao.mapper;

import nl.aimsites.nzbvuq.datastorage.dao.mapper.EntityMapper;
import nl.aimsites.nzbvuq.datastorage.dao.mapper.TileMapper;
import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.*;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TileMapperTest {
  @Test
  void testMapRoom() {
    // Arrange

    // Act
    TileDto tileDto = TileMapper.map("Room", 20, 20, 5);

    // Assert
    Assertions.assertTrue(tileDto instanceof RoomDto);
  }

  @Test
  void testMapCorridor() {
    // Arrange

    // Act
    TileDto tileDto = TileMapper.map("Corridor", 20, 20, 5);

    // Assert
    Assertions.assertTrue(tileDto instanceof CorridorDto);
  }

  @Test
  void testMapField() {
    // Arrange

    // Act
    TileDto tileDto = TileMapper.map("Field", 20, 20, 5);

    // Assert
    Assertions.assertTrue(tileDto instanceof FieldDto);
  }

  @Test
  void testMapForest() {
    // Arrange

    // Act
    TileDto tileDto = TileMapper.map("Forest", 20, 20, 5);

    // Assert
    Assertions.assertTrue(tileDto instanceof ForestDto);
  }

  @Test
  void testMapNull() {
    // Arrange

    // Act
    TileDto tileDto = TileMapper.map("niks", 20, 20, 20);

    // Assert
    Assertions.assertNull(tileDto);
  }
}
