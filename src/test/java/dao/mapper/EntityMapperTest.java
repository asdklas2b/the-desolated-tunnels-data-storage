package dao.mapper;

import nl.aimsites.nzbvuq.datastorage.dao.mapper.AIMapper;
import nl.aimsites.nzbvuq.datastorage.dao.mapper.EntityMapper;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.ChestDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.ArmorDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.SpecialDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.StrengthDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.WeaponDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.special.FlagDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EntityMapperTest {
  @Test
  void testMapArmor() {
    // Arrange

    // Act
    EntityDto entityDto = EntityMapper.map("Armor");

    // Assert
    Assertions.assertTrue(entityDto instanceof ArmorDto);
  }

  @Test
  void testMapStrength() {
    // Arrange

    // Act
    EntityDto entityDto = EntityMapper.map("Strength");

    // Assert
    Assertions.assertTrue(entityDto instanceof StrengthDto);
  }

  @Test
  void testMapSpecial() {
    // Arrange

    // Act
    EntityDto entityDto = EntityMapper.map("Special");

    // Assert
    Assertions.assertTrue(entityDto instanceof SpecialDto);
  }

  @Test
  void testMapWeapon() {
    // Arrange

    // Act
    EntityDto entityDto = EntityMapper.map("Weapon");

    // Assert
    Assertions.assertTrue(entityDto instanceof WeaponDto);
  }

  @Test
  void testMapChest() {
    // Arrange

    // Act
    EntityDto entityDto = EntityMapper.map("Chest");

    // Assert
    Assertions.assertTrue(entityDto instanceof ChestDto);
  }

  @Test
  void testMapFlag() {
    // Arrange

    // Act
    EntityDto entityDto = EntityMapper.map("Flag");

    // Assert
    Assertions.assertTrue(entityDto instanceof FlagDto);
  }

  @Test
  void testMapNull() {
    // Arrange

    // Act
    EntityDto entityDto = EntityMapper.map("niks");

    // Assert
    Assertions.assertNull(entityDto);
  }
}
