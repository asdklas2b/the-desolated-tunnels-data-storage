package dao.imp;

import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.impl.PeerDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;
import nl.jiankai.mapper.ResultSetMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;

public class PeerDaoTest {
  private PeerDaoImpl dao;
  private DatabaseConnection connection;
  private DatabaseStatement statement;
  private ResultSetMapper mapper;

  private PeerDto peerDto;
  private List<PeerDto> peerDtos;

  @BeforeEach
  void setup() {
    dao = new PeerDaoImpl();
      peerDtos = new ArrayList<>();
      peerDto = new PeerDto();
    peerDto.setId("id");
    peerDtos.add(peerDto);

    // Connection
    connection = Mockito.mock(DatabaseConnection.class);

    // Statement
    statement = Mockito.mock(DatabaseStatement.class);
    Mockito.when(connection.createStatement(Mockito.anyString())).thenReturn(statement);

    // Mapper
    mapper = Mockito.mock(ResultSetMapper.class);
    Mockito.when(mapper.map(Mockito.any(), eq(PeerDto.class))).thenReturn(peerDtos);

    dao.setConnection(connection);
    dao.setMapper(mapper);
  }

  @Test
  void testCreate() throws Exception {
    // Arrange
    // Empty

    // Act
    dao.create(peerDto);

    // Assert
    Mockito.verify(connection).createStatement(Mockito.anyString());
    Mockito.verify(statement).setString(Mockito.eq(1), Mockito.anyString());
    Mockito.verify(statement).executeQuery();
  }

  @Test
  void testIfReadIsCalling() throws Exception {
    // Arrange
    // Empty

    // Act
    dao.read();

    // Assert
    Mockito.verify(connection).createStatement(Mockito.anyString());
    Mockito.verify(statement).executeQuery();
  }

  @Test
  void testIfReadIsReturning() {
    // Arrange

    // Act
    var result = dao.read();

    // Assert
    Assertions.assertEquals(peerDtos, result);
  }

  @Test
  void testIfFindIsCalling() {
    // Arrange

    // Act
    dao.find("");

    // Assert
    Mockito.verify(connection).createStatement(Mockito.anyString());
    Mockito.verify(statement).setString(Mockito.eq(1), Mockito.any());
    Mockito.verify(statement).executeQuery();
    Mockito.verify(mapper).map(Mockito.any(), Mockito.any());
  }

  @Test
  void testIfFindIsReturning() {
    // Arrange

    // Act
    var result = dao.find("");

    // Assert
    Assertions.assertEquals(peerDto, result);
  }

  @Test
  void testUpdate() {
    // Arrange

    // Act
    dao.update("", peerDto);

    // Assert
    Mockito.verify(connection).createStatement(Mockito.anyString());
    Mockito.verify(statement).setString(Mockito.eq(1), Mockito.anyString());
    Mockito.verify(statement).setString(Mockito.eq(2), Mockito.anyString());
    Mockito.verify(statement).execute();
  }

  @Test
  void testDelete() {
    // Arrange

    // Act
    dao.delete("");

    // Assert
    Mockito.verify(connection).createStatement(Mockito.anyString());
    Mockito.verify(statement).setString(Mockito.eq(1), Mockito.anyString());
    Mockito.verify(statement).execute();
  }
}
