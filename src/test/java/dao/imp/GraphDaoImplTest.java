package dao.imp;

import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.impl.GraphDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dao.impl.TileDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.graph.GraphDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.RoomDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;
import nl.jiankai.mapper.ResultSetMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class GraphDaoImplTest {
  private ResultSetMapper mapper;
  private DatabaseConnection databaseConnection;
  private DatabaseStatement databaseStatement;
  private ResultSet mockedResultSet;
  private TileDaoImpl tileDao;
  private GraphDaoImpl graphDao;
  private List<GraphDto> graphDtos;

  @BeforeEach
  void setup() throws SQLException {
    mapper = Mockito.mock(ResultSetMapper.class);
    databaseConnection = Mockito.mock(DatabaseConnection.class);
    databaseStatement = Mockito.mock(DatabaseStatement.class);
    mockedResultSet = Mockito.mock(ResultSet.class);
    Mockito.when(databaseConnection.createStatement(Mockito.anyString())).thenReturn(databaseStatement);
    Mockito.when(databaseStatement.executeQuery()).thenReturn(mockedResultSet);
    Mockito.when(mockedResultSet.next()).thenReturn(true).thenReturn(false);
    tileDao = Mockito.mock(TileDaoImpl.class);

    graphDao = new GraphDaoImpl();
    graphDao.setTileDao(tileDao);
    graphDao.setConnection(databaseConnection);
    graphDao.setMapper(mapper);

    graphDtos = new ArrayList<>();
    PointDto pointDto = new PointDto(20, 20);
    TileDto tileDto = new RoomDto(pointDto, 2);
    GraphDto graphDto = new GraphDto();
    graphDto.setTiles(new TileDto[22][22]);
    graphDto.addTile(tileDto);
    graphDtos.add(graphDto);
  }

  @Test
  void testRead() {
    // Arrange
    Mockito.when(mapper.map(Mockito.any(), Mockito.eq(GraphDto.class))).thenReturn(graphDtos);

    // Act
    List<GraphDto> response = graphDao.read();

    // Assert
    Mockito.verify(databaseConnection).connect();
    Mockito.verify(tileDao).read(Mockito.any());
    Mockito.verify(databaseConnection).close();
    Assertions.assertEquals(1, response.size());
  }

  @Test
  void testCreate() {
    // Arrange

    // Act
    graphDao.create(graphDtos.get(0));

    // Assert
    Mockito.verify(databaseConnection).createStatement(Mockito.anyString());
    Mockito.verify(databaseStatement, Mockito.atLeast(2))
        .setInt(Mockito.anyInt(), Mockito.anyInt());
    Mockito.verify(databaseStatement).execute();
  }

  @Test
  void testDelete() {
    // Arrange
    GraphDto graphDto = new GraphDto(0, 0, 30, 30);
    graphDto.addTile(new RoomDto(new PointDto(20, 20), 20));

    // Act
    graphDao.delete();

    // Assert
    Mockito.verify(databaseConnection, Mockito.atLeast(2)).connect();
    Mockito.verify(databaseConnection, Mockito.atLeast(2)).createStatement(Mockito.anyString());
    Mockito.verify(databaseStatement).execute();
    Mockito.verify(databaseConnection).close();
  }
}
