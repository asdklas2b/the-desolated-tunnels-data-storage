package dao.imp;

import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.impl.ActionResponseDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.RoomDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class ActionResponseDaoImplTest {
    private ActionResponseDaoImpl actionResponseDao;
    private DatabaseConnection mockedConnection;
    private DatabaseStatement mockedStatement;

    @BeforeEach
    void setup() {
        actionResponseDao = new ActionResponseDaoImpl();
        mockedConnection = Mockito.mock(DatabaseConnection.class);
        mockedStatement = Mockito.mock(DatabaseStatement.class);
        Mockito.when(mockedConnection.createStatement(Mockito.anyString())).thenReturn(mockedStatement);
        actionResponseDao.setConnection(mockedConnection);
    }

    @Test
    void testCreate() {
        // Arrange
        RoomDto room = new RoomDto(new PointDto(20, 20), 5);

        // Act
        actionResponseDao.create(room, "test", "test");

        // Assert
        Mockito.verify(mockedConnection).connect();
        Mockito.verify(mockedStatement, Mockito.atLeast(2)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockedStatement, Mockito.atLeast(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockedStatement).execute();
        Mockito.verify(mockedConnection).close();
    }
}
