package dao.imp;

import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.impl.EntityDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dao.impl.TileDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.graph.GraphDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.FieldDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;
import nl.jiankai.mapper.ResultSetMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class TileDaoImplTest {
  private TileDaoImpl tileDao;
  private DatabaseConnection databaseConnection;
  private DatabaseStatement databaseStatement;
  private ResultSet resultSet;
  private ResultSetMapper mapper;
  private EntityDaoImpl entityDao;

  @BeforeEach
  void setup() throws SQLException {
    tileDao = new TileDaoImpl();
    databaseConnection = Mockito.mock(DatabaseConnection.class);
    databaseStatement = Mockito.mock(DatabaseStatement.class);
    resultSet = Mockito.mock(ResultSet.class);
    mapper = Mockito.mock(ResultSetMapper.class);
    entityDao = Mockito.mock(EntityDaoImpl.class);

    Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
    Mockito.when(resultSet.getString(Mockito.anyString())).thenReturn("Room");
    Mockito.when(resultSet.getInt(Mockito.anyString())).thenReturn(0);
    Mockito.when(databaseStatement.executeQuery()).thenReturn(resultSet);
    Mockito.when(databaseConnection.createStatement(Mockito.anyString()))
        .thenReturn(databaseStatement);

    tileDao.setConnection(databaseConnection);
    tileDao.setMapper(mapper);
    tileDao.setEntityDao(entityDao);
  }

  @Test
  void testRead() {
    // Arrange
    GraphDto graphDto = new GraphDto();

    // Act
    List<TileDto> response = tileDao.read(graphDto);

    // Assert
    Mockito.verify(databaseConnection).connect();
    Mockito.verify(databaseConnection).createStatement(Mockito.anyString());
    Mockito.verify(databaseStatement, Mockito.atLeast(4))
        .setInt(Mockito.anyInt(), Mockito.anyInt());
    Mockito.verify(databaseStatement).executeQuery();
    Assertions.assertEquals(1, response.size());
  }

  @Test
  void testCreate() {
    // Arrange
    TileDto tile = new FieldDto(new PointDto(20, 20), 5);

    // Act
    tileDao.create(tile);

    // Assert
    Mockito.verify(databaseConnection).connect();
    Mockito.verify(databaseStatement, Mockito.atLeast(1)).setString(Mockito.anyInt(), Mockito.anyString());
    Mockito.verify(databaseStatement, Mockito.atLeast(4)).setInt(Mockito.anyInt(), Mockito.anyInt());
    Mockito.verify(databaseStatement).execute();
    Mockito.verify(databaseConnection).close();
  }

  @Test
  void testDelete() {
    // Arrange
    TileDto tile = new FieldDto(new PointDto(20, 20), 5);

    // Act
    tileDao.delete(tile);

    // Assert
    Mockito.verify(databaseConnection).connect();
    Mockito.verify(databaseStatement, Mockito.atLeast(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    Mockito.verify(databaseStatement).execute();
    Mockito.verify(databaseConnection).close();
  }
}
