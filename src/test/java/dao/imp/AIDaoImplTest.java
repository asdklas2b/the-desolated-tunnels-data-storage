package dao.imp;

import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.impl.AIDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AgentDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

class AIDaoImplTest {
    private AIDaoImpl aiDao;
    private DatabaseConnection mockedConnection;
    private DatabaseStatement mockedStatement;
    private ResultSet mockedResultSet;
    private AIDto exampleAIDto;

    @BeforeEach
    void setup() throws SQLException {
        aiDao = new AIDaoImpl();

        mockedConnection = Mockito.mock(DatabaseConnection.class);
        mockedStatement = Mockito.mock(DatabaseStatement.class);
        mockedResultSet = Mockito.mock(ResultSet.class);
        Mockito.when(mockedConnection.createStatement(Mockito.anyString())).thenReturn(mockedStatement);
        Mockito.when(mockedStatement.executeQuery()).thenReturn(mockedResultSet);
        Mockito.when(mockedResultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(mockedResultSet.getString(Mockito.anyString())).thenReturn("value");
        Mockito.when(mockedResultSet.getString("type")).thenReturn("Agent");

        exampleAIDto = new AgentDto();
        exampleAIDto.setName("name");
        exampleAIDto.setCommand("if this then that");
        PeerDto peerDto = new PeerDto();
        peerDto.setId("xxx");
        exampleAIDto.setPeer(peerDto);
        aiDao.setConnection(mockedConnection);
    }

    @Test
    void testCreate() {
        // Arrange

        // Act
        aiDao.create(exampleAIDto);

        // Assert
        Mockito.verify(mockedConnection).connect();
        Mockito.verify(mockedStatement, Mockito.atLeast(3)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockedStatement).execute();
        Mockito.verify(mockedConnection).close();
    }

    @Test
    void testRead() {
        // Arrange

        // Act
        List<AIDto> response = aiDao.read();

        // Assert
        Mockito.verify(mockedConnection).connect();
        Mockito.verify(mockedStatement).executeQuery();
        Mockito.verify(mockedConnection).close();
        Assertions.assertEquals(1, response.size());
    }

    @Test
    void testDelete() {
        // Arrange

        // Act
        aiDao.delete(exampleAIDto);

        // Assert
        Mockito.verify(mockedConnection).connect();
        Mockito.verify(mockedStatement, Mockito.atLeast(2)).setString(Mockito.anyInt(), Mockito.anyString());
        Mockito.verify(mockedStatement).execute();
        Mockito.verify(mockedConnection).close();
    }
}