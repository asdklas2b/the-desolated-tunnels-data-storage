package dao.imp;

import nl.aimsites.nzbvuq.datastorage.dao.ActionResponseDao;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.impl.ActionResponseDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dao.impl.BaseItemDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.ArmorDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.BaseItemDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.RoomDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;
import nl.jiankai.mapper.ResultSetMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BaseItemDaoImplTest {
    private BaseItemDaoImpl baseItemDao;
    private DatabaseConnection mockedDatabaseConnection;
    private DatabaseStatement mockedStatement;
    private ArmorDto exampleItem;
    private ResultSetMapper mockedMapper;
    private BaseItemDto item;
    private TileDto tile;

    @BeforeEach
    void setup() throws SQLException {
        this.exampleItem = new ArmorDto();
        this.mockedDatabaseConnection = Mockito.mock(DatabaseConnection.class);
        this.mockedStatement = Mockito.mock(DatabaseStatement.class);

        mockedMapper = Mockito.mock(ResultSetMapper.class);
        ResultSet mockedResultSet = Mockito.mock(ResultSet.class);

        var list = new ArrayList<>();
        list.add(exampleItem);

        Mockito.when(mockedDatabaseConnection.createStatement(Mockito.anyString())).thenReturn(mockedStatement);
        Mockito.when(mockedStatement.executeQuery()).thenReturn(mockedResultSet);
        Mockito.doNothing().when(mockedStatement).execute();
        Mockito.when(mockedMapper.map(Mockito.any(), Mockito.any())).thenReturn(list);
        Mockito.when(mockedResultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(mockedResultSet.getString(Mockito.anyString())).thenReturn("value");
        Mockito.when(mockedResultSet.getInt(Mockito.anyString())).thenReturn(20);

        item = new ArmorDto();
        tile = new RoomDto(new PointDto(0, 0), 1);

        this.baseItemDao = new BaseItemDaoImpl();
        baseItemDao.setConnection(mockedDatabaseConnection);
        baseItemDao.setMapper(mockedMapper);
    }

    @Test
    void testFindDetails() {
        // Arrange
        int value = 20;

        // Act
        BaseItemDto response = baseItemDao.findDetails(exampleItem, value, value);

        // Assert
        Mockito.verify(mockedDatabaseConnection).createStatement(Mockito.anyString());
        Mockito.verify(mockedStatement).setTableName("Armor");
        Mockito.verify(mockedStatement, Mockito.atLeast(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockedStatement).executeQuery();
        Assertions.assertEquals(response, exampleItem);
    }

    @Test
    void testFindActionResponses() {
        // Arrange
        int value = 20;

        // Act
        Map<String, String> response = baseItemDao.findActionResponses(value, value);

        // Assert
        Mockito.verify(mockedDatabaseConnection).createStatement(Mockito.anyString());
        Mockito.verify(mockedStatement, Mockito.atLeast(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockedStatement).executeQuery();
        Assertions.assertEquals(1, response.size());
    }

    @Test
    void testCreateBaseItem(){
        //Arrange
        this.baseItemDao = Mockito.spy(new BaseItemDaoImpl());
        baseItemDao.setConnection(mockedDatabaseConnection);
        baseItemDao.setMapper(mockedMapper);
        Mockito.doNothing().when(baseItemDao).createSubItem(Mockito.any(), Mockito.any());
        Mockito.doNothing().when(baseItemDao).createActionResponses(Mockito.any(), Mockito.any());

        BaseItemDto item = new ArmorDto();
        TileDto tile = new RoomDto(new PointDto(0, 0), 1);

        //Act
        baseItemDao.create(tile, item);

        //Assert
        Mockito.verify(mockedDatabaseConnection).connect();
        Mockito.verify(mockedDatabaseConnection).createStatement(Mockito.anyString());
        Mockito.verify(mockedStatement, Mockito.atLeast(3)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockedStatement).execute();

        Mockito.verify(baseItemDao).createSubItem(Mockito.any(), Mockito.any());
        Mockito.verify(baseItemDao).createActionResponses(Mockito.any(), Mockito.any());
        Mockito.verify(mockedDatabaseConnection).close();
    }

    @Test
    void testCreateActionResponses(){
        //Arrange
        Map<String,String> actionResponses = new HashMap<>();
        actionResponses.put("action", "response");
        item.setActionResponses(actionResponses);

        ActionResponseDao actionResponseDao = Mockito.mock(ActionResponseDaoImpl.class);
        Mockito.doNothing().when(actionResponseDao).create(Mockito.any(TileDto.class), Mockito.anyString(), Mockito.anyString());
        baseItemDao.setActionResponseDao(actionResponseDao);

        //Act
        baseItemDao.createActionResponses(tile, item);

        //Assert
        Mockito.verify(actionResponseDao).create(Mockito.any(TileDto.class), Mockito.anyString(), Mockito.anyString());
    }

    @Test
    void testSetCoordinates(){
        //Arrange
        Mockito.doNothing().when(mockedStatement).setInt(Mockito.anyInt(), Mockito.anyInt());

        //Act
        baseItemDao.setCoordinates(tile, mockedStatement, 0);


        // Assert
        Mockito.verify(mockedStatement, Mockito.atLeast(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
    }

    @Test
    void testDelete() {
        // Arrange
        int value = 20;

        // Act
        baseItemDao.delete(exampleItem, value, value);

        //Assert
        Mockito.verify(mockedDatabaseConnection).connect();
        Mockito.verify(mockedDatabaseConnection).createStatement(Mockito.anyString());
        Mockito.verify(mockedStatement, Mockito.atLeast(2)).setInt(Mockito.anyInt(), Mockito.anyInt());
        Mockito.verify(mockedStatement).execute();
        Mockito.verify(mockedDatabaseConnection).close();
    }
}


