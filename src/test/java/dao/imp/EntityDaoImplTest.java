package dao.imp;

import nl.aimsites.nzbvuq.datastorage.dao.BaseItemDao;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.impl.EntityDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.ArmorDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.WeaponDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.RoomDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public class EntityDaoImplTest {
  private EntityDaoImpl entityDaoImpl;

  private DatabaseConnection mockedDatabaseConnection;
  private DatabaseStatement mockedDatabaseStatement;
  private BaseItemDao mockedBaseItemDao;
  private ResultSet mockedResultSet;

  @BeforeEach
  void setup() throws SQLException {
    entityDaoImpl = new EntityDaoImpl();
    mockedDatabaseConnection = Mockito.mock(DatabaseConnection.class);
    mockedDatabaseStatement = Mockito.mock(DatabaseStatement.class);
    mockedResultSet = Mockito.mock(ResultSet.class);
    mockedBaseItemDao = Mockito.mock(BaseItemDao.class);

    Mockito.when(mockedDatabaseConnection.createStatement(Mockito.anyString())).thenReturn(mockedDatabaseStatement);
    Mockito.when(mockedDatabaseStatement.executeQuery()).thenReturn(mockedResultSet);
    Mockito.when(mockedResultSet.next()).thenReturn(true).thenReturn(false);
    Mockito.when(mockedResultSet.getString("name")).thenReturn("Armor");
    Mockito.when(mockedResultSet.getInt(Mockito.anyString())).thenReturn(200);

    Mockito.when(mockedBaseItemDao.find(Mockito.any(), Mockito.anyInt(), Mockito.anyInt())).thenReturn(new ArmorDto());

    entityDaoImpl.setConnection(mockedDatabaseConnection);
    entityDaoImpl.setBaseItemDao(mockedBaseItemDao);
  }

  @Test
  void testRead() {
    // Arrange
    PointDto pointDto = new PointDto(20, 20);
    TileDto tileDto = new RoomDto(pointDto, 2);

    // Act
    List<EntityDto> response = entityDaoImpl.read(tileDto);

    // Assert
    Mockito.verify(mockedDatabaseConnection).connect();
    Mockito.verify(mockedDatabaseConnection).createStatement(Mockito.anyString());
    Mockito.verify(mockedDatabaseStatement, Mockito.atLeast(2))
        .setInt(Mockito.anyInt(), Mockito.anyInt());
    Mockito.verify(mockedDatabaseStatement).executeQuery();
    Mockito.verify(mockedDatabaseConnection).close();
    Assertions.assertEquals(1, response.size());
  }

  @Test
  void testCreate() {
    // TODO
  }

  @Test
  void testDelete() {
    // Arrange
    EntityDto entityDto = new WeaponDto();

    // Act
    entityDaoImpl.delete(20, 20, entityDto);

    // Assert
    Mockito.verify(mockedDatabaseConnection).connect();
    Mockito.verify(mockedDatabaseConnection).createStatement(Mockito.anyString());
    Mockito.verify(mockedDatabaseStatement, Mockito.atLeast(2))
            .setInt(Mockito.anyInt(), Mockito.anyInt());
    Mockito.verify(mockedDatabaseStatement).execute();
    Mockito.verify(mockedDatabaseConnection).close();
  }
}
