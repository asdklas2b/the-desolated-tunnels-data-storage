package nl.aimsites.nzbvuq.datastorage.dto.graph;

import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;

import java.util.Arrays;
import java.util.Optional;

/**
 * This is a chunk of the world, it contains a 2D array of tiles where entities can reside in.
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class GraphDto {
  private TileDto[][] tileDtos;
  private int startX;
  private int startY;
  private int size;
  private long seed;

  /**
   * Constructs an instance of the class
   *
   * @param startX starting X position of the chunk
   * @param startY starting Y position of the chunk
   * @param size size of the graph
   * @param seed the seed that was used to generate the graph
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public GraphDto(int startX, int startY, int size, long seed) {
    this.startX = startX;
    this.startY = startY;
    this.size = size;
    tileDtos = new TileDto[size][size];
    this.seed = seed;
  }

  /**
   * Constructs an empty instance of the class (used for data storage)
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public GraphDto() {}

  /**
   * Adds a tile in the graph on the position of their coordinate
   *
   * @param tileDto the tile to be added
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public void addTile(TileDto tileDto) {
    if (tileDto == null) {
      throw new IllegalArgumentException("No tile");
    }

    tileDtos[tileDto.getY() - startY][tileDto.getX() - startX] = tileDto;
  }

  public TileDto[][] getTiles() {
    return tileDtos;
  }

  public void setTiles(TileDto[][] tileDtos) {
    this.tileDtos = tileDtos;
  }

  public Optional<TileDto> findTile(int x, int y) {
    return Optional.of(tileDtos[y][x]);
  }

  public int getStartX() {
    return startX;
  }

  public void setStartX(int startX) {
    this.startX = startX;
  }

  public int getStartY() {
    return startY;
  }

  public void setStartY(int startY) {
    this.startY = startY;
  }

  public int getSize() {
    return size;
  }

  public void setSize(int size) {
    this.size = size;
  }

  public long getSeed() {
    return seed;
  }

  public void setSeed(int seed) {
    this.seed = seed;
  }

  @Override
  public String toString() {
    return "Graph{"
        + "tiles="
        + Arrays.toString(tileDtos)
        + ", startX="
        + startX
        + ", startY="
        + startY
        + ", size="
        + size
        + ", seed="
        + seed
        + '}';
  }
}
