package nl.aimsites.nzbvuq.datastorage.dto.entities.items;

/**
 * The special item type are for items that do special things. These can not be classified in the
 * category Armor, Weapon or Strength
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class SpecialDto extends BaseItemDto {
  @Override
  public String look() {
    if (!actionResponses.containsKey("look")) {
      return super.look();
    }

    return actionResponses.get("look");
  }

  @Override
  public String pickUp() {
    if (!actionResponses.containsKey("pick up")) {
      return super.pickUp();
    }

    return actionResponses.get("pick up");
  }

  @Override
  public String drop() {
    if (!actionResponses.containsKey("drop")) {
      return super.drop();
    }

    return actionResponses.get("drop");
  }

  @Override
  public String consume() {
    if (!actionResponses.containsKey("consume")) {
      return super.consume();
    }

    return actionResponses.get("consume");
  }

  @Override
  public String use() {
    if (!actionResponses.containsKey("use")) {
      return super.use();
    }

    return actionResponses.get("use");
  }
}
