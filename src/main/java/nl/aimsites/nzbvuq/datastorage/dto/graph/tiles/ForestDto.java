package nl.aimsites.nzbvuq.datastorage.dto.graph.tiles;

import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;

/**
 * A forest in the world
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class ForestDto extends UnreachableTileDto {

  /**
   * Constructs an instance of the class
   *
   * @param coordinates the coordinates of the tile
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public ForestDto(PointDto coordinates) {
    super(coordinates);
  }
}
