package nl.aimsites.nzbvuq.datastorage.dto;

/**
 * The type Team.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class TeamDto {
  private int id;

  public TeamDto(int id) {
    this.id = id;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }
}
