package nl.aimsites.nzbvuq.datastorage.dto.entities;

/**
 * Base class for an entity that can be found in the game world
 *
 * @author Vick Top (v.top@student.han.nl)
 */
public abstract class EntityDto {
  protected String name;

  /**
   * This this constructor makes a new instance of an Entity object
   *
   * @author Vick Top (v.top@student.han.nl)
   */
  protected EntityDto() {}

  /**
   * This this constructor makes a new instance of an Entity object. Also sets the name
   *
   * @author Vick Top (v.top@student.han.nl)
   */
  protected EntityDto(String name) {
    this.name = name;
  }

  /**
   * Gets name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets name.
   *
   * @param name the name
   */
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Entity{" + "name='" + name + '\'' + '}';
  }

  public String getTableName() {
    String className = this.getClass().getSimpleName();
    return className.replace("Dto", "");
  }
}
