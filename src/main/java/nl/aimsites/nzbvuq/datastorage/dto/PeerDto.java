package nl.aimsites.nzbvuq.datastorage.dto;

import nl.jiankai.annotations.Column;

/**
 * The type Peer.
 *
 * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
 */
public class PeerDto {
  private Character character;

  @Column(name = "playerId")
  private String id;

  private int power;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public Character getCharacter() {
    return character;
  }

  public void setCharacter(Character character) {
    this.character = character;
  }

  public int getPower() {
    return power;
  }

  public void setPower(int power) {
    this.power = power;
  }

  @Override
  public String toString() {
    return "Player{" + "character=" + character + ", id='" + id + '\'' + '}';
  }
}
