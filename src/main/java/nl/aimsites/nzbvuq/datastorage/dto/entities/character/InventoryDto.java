package nl.aimsites.nzbvuq.datastorage.dto.entities.character;

import nl.aimsites.nzbvuq.datastorage.dto.entities.items.BaseItemDto;

import java.util.ArrayList;
import java.util.List;

/**
 * A place for entities to store items.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class InventoryDto {
    private List<BaseItemDto> items = new ArrayList<>();
    private int maxSize = 10;

    /**
     * Inventory constructor
     *
     * @author Jorrit Schepers (J.Schepers3@student.han.nl)
     */
    public InventoryDto() {
    }

    public List<BaseItemDto> getItems() {
        return items;
    }

    public int getMaxSize() {
        return maxSize;
    }

    public void setItems(List<BaseItemDto> items) {
        this.items = items;
    }

    public void setMaxSize(int maxSize) {
        this.maxSize = maxSize;
    }
}