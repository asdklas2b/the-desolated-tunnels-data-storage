package nl.aimsites.nzbvuq.datastorage.dto.ai;

import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;

/** The type Ai. */
public abstract class AIDto extends EntityDto {
  protected String type;
  private PeerDto peer;
  private String command;

  public String getCommand() {
    return command;
  }

  public void setCommand(String command) {
    this.command = command;
  }

  public String getType() {
    return type;
  }

  public void setType(String type) {
    this.type = type;
  }

  public PeerDto getPeer() {
    return peer;
  }

  public void setPeer(PeerDto peer) {
    this.peer = peer;
  }
}
