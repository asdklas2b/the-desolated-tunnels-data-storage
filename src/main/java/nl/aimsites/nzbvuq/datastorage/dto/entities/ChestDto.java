package nl.aimsites.nzbvuq.datastorage.dto.entities;

/**
 * The base class for a chest in the game world. A chest can contain attributes.
 *
 * @author Vick Top (v.top@student.han.nl)
 */
public class ChestDto extends EntityDto {

  /**
   * This constructor makes a new instance of a Chest object
   *
   * @author Vick Top (v.top@student.han.nl)
   */
  public ChestDto() {
    super.setName("Chest");
  }
}
