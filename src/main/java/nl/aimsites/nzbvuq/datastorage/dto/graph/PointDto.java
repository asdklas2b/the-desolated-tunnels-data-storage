package nl.aimsites.nzbvuq.datastorage.dto.graph;

import java.util.Objects;

/**
 * Coordinates of a tileDto
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class PointDto {
  private final int x;
  private final int y;

  public PointDto(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return x;
  }

  public int getY() {
    return y;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    PointDto pointDto = (PointDto) o;
    return x == pointDto.x && y == pointDto.y;
  }

  @Override
  public int hashCode() {
    return Objects.hash(x, y);
  }
}
