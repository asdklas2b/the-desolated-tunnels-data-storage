package nl.aimsites.nzbvuq.datastorage.dto.graph;

import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;

import java.util.Objects;

/**
 * This class is the "connection" between two tiles.
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class EdgeDto {
  private TileDto destinationTileDto;

  /**
   * Instantiates a new Edge.
   *
   * @param destinationTileDto the destination tileDto
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public EdgeDto(TileDto destinationTileDto) {
    setDestinationTile(destinationTileDto);
  }

  public TileDto getDestinationTile() {
    return destinationTileDto;
  }

  public void setDestinationTile(TileDto destinationTileDto) {
    if (destinationTileDto == null) {
      throw new IllegalArgumentException("Destination Tile can not be null!");
    }

    this.destinationTileDto = destinationTileDto;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    EdgeDto edgeDto = (EdgeDto) o;
    return destinationTileDto.equals(edgeDto.destinationTileDto);
  }

  @Override
  public int hashCode() {
    return Objects.hash(destinationTileDto);
  }
}
