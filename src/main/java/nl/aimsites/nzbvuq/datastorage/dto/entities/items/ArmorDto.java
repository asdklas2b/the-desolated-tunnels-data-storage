package nl.aimsites.nzbvuq.datastorage.dto.entities.items;

/**
 * An item type for armor. Armor will defend the player from enemy attacks.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class ArmorDto extends BaseItemDto implements Cloneable {
  private int strengthAddition;
  private int maxStrengthAddition;

  @Override
  public String look() {
    if (!actionResponses.containsKey("look")) {
      return super.look();
    }

    return actionResponses.get("look");
  }

  @Override
  public String pickUp() {
    if (!actionResponses.containsKey("pick up")) {
      return super.pickUp();
    }

    return actionResponses.get("pick up");
  }

  @Override
  public String drop() {
    if (!actionResponses.containsKey("drop")) {
      return super.drop();
    }

    return actionResponses.get("drop");
  }

  public int getStrengthAddition() {
    return strengthAddition;
  }

  public void setStrengthAddition(int strengthAddition) {
    this.strengthAddition = strengthAddition;
  }

  public int getMaxStrengthAddition() {
    return maxStrengthAddition;
  }

  public void setMaxStrengthAddition(int maxStrengthAddition) {
    this.maxStrengthAddition = maxStrengthAddition;
  }
}
