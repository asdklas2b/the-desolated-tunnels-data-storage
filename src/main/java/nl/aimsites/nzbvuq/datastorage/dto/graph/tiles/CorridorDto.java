package nl.aimsites.nzbvuq.datastorage.dto.graph.tiles;

import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;

/**
 * A corridor in a castle
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class CorridorDto extends ReachableTileDto {

  /**
   * Constructs an instance of the class
   *
   * @param coordinates coordinates of the corridor
   * @param costOfTraversal cost needed to walk on corridor
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public CorridorDto(PointDto coordinates, int costOfTraversal) {
    super(coordinates, costOfTraversal);
  }
}
