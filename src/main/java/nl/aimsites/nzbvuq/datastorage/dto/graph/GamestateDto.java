package nl.aimsites.nzbvuq.datastorage.dto.graph;

import java.util.List;

/**
 * The type Gamestate.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class GamestateDto {
  private List<GraphDto> graphs;

  public List<GraphDto> getGraphs() {
    return graphs;
  }

  public void setGraphs(List<GraphDto> graphs) {
    this.graphs = graphs;
  }

  public String toString() {
    return "Gamestate{" + "graphs=" + graphs + '}';
  }
}
