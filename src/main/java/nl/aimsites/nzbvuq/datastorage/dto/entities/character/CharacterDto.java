package nl.aimsites.nzbvuq.datastorage.dto.entities.character;

import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.WeaponDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.ReachableTileDto;

/**
 * A Character is a entity that can perform actions and change the state of the game.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 */
public abstract class CharacterDto extends EntityDto {

    private int peerID;
    private int strength;
    private int maxStrength;
    private WeaponDto equippedWeapon;
    private InventoryDto inventory;
    private String identificationKey;
    private ReachableTileDto currentPosition;

    /**
     * This constructor constructs the character class
     *
     * @author Feida Wei (FD.Wei@student.han.nl)
     */
    protected CharacterDto() {}

    public int getStrength() {
        return strength;
    }

    public void setMaxStrength(int maxStrength) {
        this.maxStrength = maxStrength;
    }

    public int getMaxStrength() {
        return maxStrength;
    }

    public InventoryDto getInventory() {
        return inventory;
    }

    public void setEquippedWeapon(WeaponDto weapon) {
        equippedWeapon = weapon;
    }

    public WeaponDto getEquippedWeapon() {
        return equippedWeapon;
    }

    public ReachableTileDto getCurrentPosition() {
        return currentPosition;
    }

    public void setCurrentPosition(ReachableTileDto currentPosition) {
        this.currentPosition = currentPosition;
    }

    public int getPeerID() {
        return peerID;
    }

    public void setIdentificationKey(String identificationKey) {
        this.identificationKey = identificationKey;
    }

    public String getIdentificationKey() {
        return identificationKey;
    }
}