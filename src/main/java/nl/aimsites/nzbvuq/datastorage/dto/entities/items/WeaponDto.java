package nl.aimsites.nzbvuq.datastorage.dto.entities.items;

/**
 * A weapon is an item that can be used to attack other characters with. This will cost strength but
 * will take more strength from the other character.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 */
public class WeaponDto extends BaseItemDto {
  private int usageCost;
  private int maxStrengthAddition;
  private int damage;

  @Override
  public String look() {
    if (!actionResponses.containsKey("look")) {
      return super.look();
    }

    return actionResponses.get("look");
  }

  @Override
  public String pickUp() {
    if (!actionResponses.containsKey("pick up")) {
      return super.pickUp();
    }

    return actionResponses.get("pick up");
  }

  @Override
  public String drop() {
    if (!actionResponses.containsKey("drop")) {
      return super.drop();
    }

    return actionResponses.get("drop");
  }

  public int getUsageCost() {
    return usageCost;
  }

  public void setUsageCost(int usageCost) {
    this.usageCost = usageCost;
  }

  public int getMaxStrengthAddition() {
    return maxStrengthAddition;
  }

  public void setMaxStrengthAddition(int maxStrengthAddition) {
    this.maxStrengthAddition = maxStrengthAddition;
  }

  public int getDamage() {
    return damage;
  }

  public void setDamage(int damage) {
    this.damage = damage;
  }
}
