package nl.aimsites.nzbvuq.datastorage.dto.ai;

/** The type Agent. */
public class AgentDto extends AIDto {
  private Character character;

  public AgentDto() {
    type = "Agent";
  }

  /**
   * Gets character.
   *
   * @return the character
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public Character getCharacter() {
    return character;
  }

  /**
   * Sets character.
   *
   * @param character the character
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setCharacter(Character character) {
    this.character = character;
  }
}
