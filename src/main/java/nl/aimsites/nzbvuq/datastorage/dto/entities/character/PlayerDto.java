package nl.aimsites.nzbvuq.datastorage.dto.entities.character;

/**
 * A Player is a character that is in the game to win. A player can be controlled by a human.
 *
 * @author Evan Franciszok (EW.Franciszok@student.han.nl)
 * @author Martijn Woolschot (M.Woolschot@student.han.nl)
 * @author Mark Jansen (MJ.Jansen4@student.han.nl)
 */
public class PlayerDto extends CharacterDto {
  private int teamNumber;

  public PlayerDto() {}

  public void setTeamNumber(int teamNumber) {
    this.teamNumber = teamNumber;
  }

  public int getTeamNumber() {
    return teamNumber;
  }
}
