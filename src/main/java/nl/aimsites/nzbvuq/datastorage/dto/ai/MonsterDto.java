package nl.aimsites.nzbvuq.datastorage.dto.ai;

/** The type Monster. */
public class MonsterDto extends AIDto {
  public MonsterDto() {
    type = "Monster";
  }
}
