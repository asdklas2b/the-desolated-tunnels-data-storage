package nl.aimsites.nzbvuq.datastorage.dto.entities.items.special;

import nl.aimsites.nzbvuq.datastorage.dto.entities.items.SpecialDto;

/**
 * The flag class is a special item type This class contains a team number to keep track which flag
 * belongs to which team.
 *
 * @author Feida Wei (FD.Wei@student.han.nl)
 */
public class FlagDto extends SpecialDto {
  private int teamNumber;

  public int getTeamNumber() {
    return teamNumber;
  }

  public void setTeamNumber(int teamNumber) {
    this.teamNumber = teamNumber;
  }

  @Override
  public String toString() {
    return "Flag{" + "teamNumber=" + teamNumber + '}';
  }
}
