package nl.aimsites.nzbvuq.datastorage.dto.graph.tiles;

import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.EdgeDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;

import java.util.ArrayList;
import java.util.List;

/**
 * A tile that is reachable by an entity
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public abstract class ReachableTileDto extends TileDto {
  private final List<EdgeDto> adjacentTiles;
  private List<EntityDto> occupyingEntities;
  private int costOfTraversal;

  /**
   * Constructs an instance of this class
   *
   * @param coordinates coordinate of the tile
   * @param costOfTraversal cost needed to walk on the tile
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  protected ReachableTileDto(PointDto coordinates, int costOfTraversal) {
    super(coordinates);
    this.costOfTraversal = costOfTraversal;
    this.adjacentTiles = new ArrayList<>();
    this.occupyingEntities = new ArrayList<>();
  }

  /**
   * Adds an edge to the list of edges
   *
   * @param edgeDto edge to be added
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public void addAdjacentTile(EdgeDto edgeDto) {
    if (edgeDto.getDestinationTile() == null) {
      throw new IllegalArgumentException("You can not add an edge that has no destination tile!");
    }
    if (this.equals(edgeDto.getDestinationTile())) {
      throw new IllegalArgumentException("You can not connect a tile to itself!");
    }

    if (this.adjacentTiles.contains(edgeDto)) {
      throw new IllegalArgumentException("Duplicate edge!");
    }

    this.adjacentTiles.add(edgeDto);
  }

  public void addEntity(EntityDto entityDto) {
    if (entityDto == null) {
      throw new IllegalArgumentException("You can not add an entity that is null!");
    }

    if (this.occupyingEntities.contains(entityDto)) {
      throw new IllegalArgumentException("Duplicate entity!");
    }

    this.occupyingEntities.add(entityDto);
  }

  public List<EdgeDto> getAdjacentTiles() {
    return List.copyOf(adjacentTiles);
  }

  public List<EntityDto> getOccupyingEntities() {
    return List.copyOf(occupyingEntities);
  }

  public void setOccupyingEntities(List<EntityDto> occupyingEntities) {
    this.occupyingEntities = occupyingEntities;
  }

  public int getCostOfTraversal() {
    return costOfTraversal;
  }

  public void setCostOfTraversal(int costOfTraversal) {
    this.costOfTraversal = costOfTraversal;
  }

  @Override
  public String toString() {
    return "ReachableTile{"
        + "adjacentTiles="
        + adjacentTiles
        + ", occupyingEntities="
        + occupyingEntities
        + ", costOfTraversal="
        + costOfTraversal
        + '}';
  }
}
