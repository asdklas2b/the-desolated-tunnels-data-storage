package nl.aimsites.nzbvuq.datastorage.dto.graph.tiles;

import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;

/**
 * A tile that is unreachable by an entity
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public abstract class UnreachableTileDto extends TileDto {

  /**
   * Instantiates a new Unreachable tile.
   *
   * @param coordinates the coordinates
   */
  public UnreachableTileDto(PointDto coordinates) {
    super(coordinates);
  }
}
