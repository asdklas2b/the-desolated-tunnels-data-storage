package nl.aimsites.nzbvuq.datastorage.dto.ai;

import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;

/** The type AI. */
public abstract class AI extends EntityDto {
  private PeerDto peer;

  public PeerDto getPeer() {
    return peer;
  }

  public void setPeer(PeerDto peer) {
    this.peer = peer;
  }
}
