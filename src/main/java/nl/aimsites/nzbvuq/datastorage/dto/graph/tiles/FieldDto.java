package nl.aimsites.nzbvuq.datastorage.dto.graph.tiles;

import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;

/**
 * A field in the world
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class FieldDto extends ReachableTileDto {

  /**
   * Constructs an instance of the class
   *
   * @param coordinates coordinate of the tile
   * @param costOfTraversal cost needed to walk on the tile
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public FieldDto(PointDto coordinates, int costOfTraversal) {
    super(coordinates, costOfTraversal);
  }
}
