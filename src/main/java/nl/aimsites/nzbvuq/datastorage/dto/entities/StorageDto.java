package nl.aimsites.nzbvuq.datastorage.dto.entities;

import nl.aimsites.nzbvuq.datastorage.dto.entities.items.BaseItemDto;

import java.util.ArrayList;
import java.util.List;

/**
 * A Storage can contain multiple items for a player to be picked up
 * @author Vick Top (VA.Top@student.han.nl)
 */
public class StorageDto {
    private List<BaseItemDto> items = new ArrayList();
    private int capacity;

    /**
     * Storage constructor
     * @author Vick Top (VA.Top@student.han.nl)
     */
    public StorageDto() {

    }

    public List<BaseItemDto> getItems() {
        return items;
    }

    public void setItems(List<BaseItemDto> items) {
        this.items = items;
    }

    public int getCapacity(){
        return capacity;
    }

}
