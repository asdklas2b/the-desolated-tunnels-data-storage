package nl.aimsites.nzbvuq.datastorage.dto;

import nl.aimsites.nzbvuq.datastorage.dto.graph.GraphDto;

import java.util.List;

/**
 * The type Gamestate.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class GamestateDto {
  private List<GraphDto> graphDtos;

  public List<GraphDto> getGraphs() {
    return graphDtos;
  }

  public void setGraphs(List<GraphDto> graphDtos) {
    this.graphDtos = graphDtos;
  }

  public String toString() {
    return "Gamestate{" + "graphs=" + graphDtos + '}';
  }
}
