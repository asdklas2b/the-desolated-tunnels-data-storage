package nl.aimsites.nzbvuq.datastorage.dto.graph.tiles;

import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;

import java.util.Objects;

/**
 * A tileDto in the world
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public abstract class TileDto {
  private PointDto coordinates;

  /**
   * Instantiates a new Tile.
   *
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public TileDto() {}

  /**
   * Instantiates a new Tile.
   *
   * @param coordinates the coordinates
   */
  public TileDto(PointDto coordinates) {
    this.coordinates = coordinates;
  }

  public PointDto getCoordinates() {
    return coordinates;
  }

  public int getX() {
    return coordinates.getX();
  }

  public int getY() {
    return coordinates.getY();
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }

    if (o == null || getClass() != o.getClass()) {
      return false;
    }

    TileDto tileDto = (TileDto) o;
    return coordinates.equals(tileDto.coordinates);
  }

  @Override
  public int hashCode() {
    return Objects.hash(coordinates);
  }

  @Override
  public String toString() {
    return "Tile{" + "coordinates=" + coordinates + '}';
  }

  public String getTableName() {
    String className = this.getClass().getSimpleName();
    return className.replace("Dto", "");
  }
}
