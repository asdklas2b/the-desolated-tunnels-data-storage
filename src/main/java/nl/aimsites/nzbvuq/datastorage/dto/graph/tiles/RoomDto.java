package nl.aimsites.nzbvuq.datastorage.dto.graph.tiles;

import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.types.RoomType;

/**
 * A room in the world
 *
 * @author Jiankai Zheng (J.Zheng1@student.han.nl)
 */
public class RoomDto extends ReachableTileDto {
  public RoomType type;

  /**
   * Constructs an instance of this class
   *
   * @param coordinates coordinate of the room
   * @param costOfTraversal the cost needed to walk on the tile
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public RoomDto(PointDto coordinates, int costOfTraversal) {
    super(coordinates, costOfTraversal);
  }

  /**
   * Instantiates a new Room.
   *
   * @param coordinates coordinate of the room
   * @param costOfTraversal the cost needed to walk on the tile
   * @param type type of the room
   * @author Jiankai Zheng (J.Zheng1@student.han.nl)
   */
  public RoomDto(PointDto coordinates, int costOfTraversal, RoomType type) {
    super(coordinates, costOfTraversal);
    this.type = type;
  }

  public RoomType getType() {
    return type;
  }

  public void setType(RoomType type) {
    this.type = type;
  }
}
