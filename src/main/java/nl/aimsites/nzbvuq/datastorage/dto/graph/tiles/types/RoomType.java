package nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.types;

/**
 * The enum Room type.
 *
 * @author Feida Wei (FD.Wei@student.han.nl)
 *     <p>Room can have only one roomtype based on the vision document. CASTLE or HOUSE
 */
public enum RoomType {
  /** Castle room type. */
  CASTLE,
  /** House room type. */
  HOUSE
}
