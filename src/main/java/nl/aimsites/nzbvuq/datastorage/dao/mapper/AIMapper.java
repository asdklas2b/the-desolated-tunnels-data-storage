package nl.aimsites.nzbvuq.datastorage.dao.mapper;

import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AgentDto;
import nl.aimsites.nzbvuq.datastorage.dto.ai.MonsterDto;

/**
 * This class is responsible for mapping database data to an AI.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class AIMapper {
    /**
     * Map ai.
     *
     * @return the entity
     * @author Stein Milder (SAJ.Milder@student.han.nl)
     */
    public static AIDto map(String type) {
        return switch (type) {
            case "Agent" -> new AgentDto();
            case "Monster" -> new MonsterDto();
            default -> null;
        };
    }
}
