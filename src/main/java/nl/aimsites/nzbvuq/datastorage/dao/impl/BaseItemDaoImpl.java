package nl.aimsites.nzbvuq.datastorage.dao.impl;

import nl.aimsites.nzbvuq.datastorage.dao.ActionResponseDao;
import nl.aimsites.nzbvuq.datastorage.dao.BaseItemDao;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.BaseItemDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;
import nl.jiankai.mapper.ResultSetMapper;
import nl.jiankai.mapper.ResultSetMapperFactory;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

/**
 * An implementation of the BaseItemDao interface
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class BaseItemDaoImpl implements BaseItemDao {
  private static final String TABLE_NAME = "item";
  private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
  private final List<String> columns = Arrays.asList("usageCost", "damage", "strengthAddition", "maxStrengthAddition", "x", "y");

  protected DatabaseConnection connection;
  protected ResultSetMapper mapper;
  private ActionResponseDao actionResponseDao;

  /**
   * This constructor constructs a base item dao implementation class
   *
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public BaseItemDaoImpl() {
    connection = new DatabaseConnection(TABLE_NAME);
    actionResponseDao = new ActionResponseDaoImpl();
    mapper = ResultSetMapperFactory.getResultSetMapperIdentity();
  }

  public void setMapper(ResultSetMapper mapper) {
    this.mapper = mapper;
  }

  public void setConnection(DatabaseConnection connection) {
    this.connection = connection;
  }

  public void setActionResponseDao(ActionResponseDao actionResponseDao) {
    this.actionResponseDao = actionResponseDao;
  }

  /**
   * Create.
   *
   * @param tile the tile
   * @param baseItem the baseItem
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public void create(TileDto tile, BaseItemDto baseItem) {
    connection.connect();

    DatabaseStatement statement = connection.createStatement("insert");
    statement.setInt(1, baseItem.getWeight());
    statement.setInt(2, tile.getX());
    statement.setInt(3, tile.getY());
    statement.execute();

    createSubItem(tile, baseItem);
    createActionResponses(tile, baseItem);

    connection.close();
  }

  /**
   * CreateSubItem.
   *
   * @param tile the tile
   * @param baseItem the baseItem
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public void createSubItem(TileDto tile, BaseItemDto baseItem) {
    DatabaseConnection typeConn = new DatabaseConnection(baseItem.getClass().getSimpleName());
    typeConn.connect();
    DatabaseStatement typeStatement = typeConn.createStatement("insert");

    try {
      Class<? extends BaseItemDto> itemClass = baseItem.getClass();
      Field[] fieldsInItem = itemClass.getDeclaredFields();

      int amountOfFields = 0;
      for (int i = 0; i < fieldsInItem.length; i++) {
        if (columns.contains(fieldsInItem[i].getName())) {
          Field currFieldInItem = getDeclaredField(itemClass, fieldsInItem[i]);
          String fieldType = fieldsInItem[i].getType().toString();

          String[] splitField = currFieldInItem.toString().split(" ");

          Object varValue;

          if (!splitField[0].equals("public")) {
            varValue = getValueOfPrivateVar(baseItem, fieldsInItem[i].getName());
          } else {
            varValue = currFieldInItem.get(baseItem);
          }

          if (fieldType.equals("int")) {
            typeStatement.setInt(i + 1, (Integer) varValue);
          } else if (fieldType.equals("String")) {
            typeStatement.setString(i + 1, (String) varValue);
          }

          amountOfFields++;
        }
      }

      setCoordinates(tile, typeStatement, amountOfFields);
      typeStatement.execute();
      typeConn.close();
    } catch (SecurityException | IllegalAccessException e) {
      LOGGER.warning("Error occurred creating a sub item");
    }
  }

  /**
   * CreateSubItem.
   *
   * @param tile the tile
   * @param baseItem the baseItem
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public void createActionResponses(TileDto tile, BaseItemDto baseItem) {
    for (Map.Entry<String, String> entry : baseItem.getActionResponses().entrySet()) {
      String key = entry.getKey();
      String value = entry.getValue();
      actionResponseDao.create(tile, key, value);
    }
  }

  /**
   * SetCoordinates.
   *
   * @param tile the tile
   * @param typeStatement the statement of the subtype
   * @param amountOfFields the amount of fields of the item
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public void setCoordinates(TileDto tile, DatabaseStatement typeStatement, int amountOfFields) {
    typeStatement.setInt(amountOfFields + 1, tile.getX());
    typeStatement.setInt(amountOfFields + 2, tile.getY());
  }

  /**
   * getDeclaredField.
   *
   * @param itemClass the class of an item
   * @param field the field
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public Field getDeclaredField(Class<? extends BaseItemDto> itemClass, Field field) {
    try {
      return itemClass.getDeclaredField(field.getName());
    } catch (NoSuchFieldException e) {
      try {
        return BaseItemDto.class.getDeclaredField(field.getName());
      } catch (NoSuchFieldException noSuchFieldException) {
        LOGGER.warning("No such field");
      }
    }
    return null;
  }

  /**
   * getValueOfPrivateVar.
   *
   * @param obj the object
   * @param variableName the name of the variable
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public Object getValueOfPrivateVar(Object obj, String variableName) {
    try {
      PropertyDescriptor pd = new PropertyDescriptor(variableName, obj.getClass());
      Method getter = pd.getReadMethod();
      return getter.invoke(obj);
    } catch (IllegalAccessException
        | IllegalArgumentException
        | IntrospectionException
        | InvocationTargetException e) {
      LOGGER.warning("Invalid argument");
    }
    return null;
  }

  /**
   * Read list.
   *
   * @return the list
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public BaseItemDto find(BaseItemDto baseItem, int x, int y) {
    connection.connect();
    baseItem = findDetails(baseItem, x, y);
    baseItem.setActionResponses(findActionResponses(x, y));
    connection.close();

    return baseItem;
  }

  @Override
  public void delete(BaseItemDto baseItemDto, int x, int y) {
    connection.connect();
    DatabaseStatement statement = connection.createStatement("delete");
    statement.setTableName(baseItemDto.getTableName());
    statement.setInt(1, x);
    statement.setInt(2, y);
    statement.execute();
    connection.close();
  }

  /**
   * Find details base item.
   *
   * @param baseItemDto the base item
   * @param x the x
   * @param y the y
   * @return the base item
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public BaseItemDto findDetails(BaseItemDto baseItemDto, int x, int y) {
    DatabaseStatement statement = connection.createStatement("details");
    statement.setTableName(baseItemDto.getTableName());
    statement.setInt(1, x);
    statement.setInt(2, y);
    ResultSet result = statement.executeQuery();
    return mapper.map(result, baseItemDto.getClass()).get(0);
  }

  /**
   * Find action responses map.
   *
   * @param x the x
   * @param y the y
   * @return the map
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public Map<String, String> findActionResponses(int x, int y) {
    Map<String, String> actionResponses = new HashMap<>();
    DatabaseStatement statement = connection.createStatement("action.response");
    statement.setInt(1, x);
    statement.setInt(2, y);
    ResultSet result = statement.executeQuery();

    try {
      while (result.next()) {
        actionResponses.put(result.getString("name"), result.getString("value"));
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }

    return actionResponses;
  }
}
