package nl.aimsites.nzbvuq.datastorage.dao.impl;

import nl.aimsites.nzbvuq.datastorage.dao.TileDao;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.mapper.TileMapper;
import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.GraphDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.ReachableTileDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;
import nl.jiankai.mapper.ResultSetMapper;
import nl.jiankai.mapper.ResultSetMapperFactory;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of the TileDao interface
 *
 * @author Pieter van der Linden (P.vanderLinden@student.han.nl) & Stein Milder
 *     (SAJ.Milder@student.han.nl)
 */
public class TileDaoImpl implements TileDao {
  private static final String TABLE_NAME = "tile";
  protected DatabaseConnection connection;
  protected ResultSetMapper mapper;
  private EntityDaoImpl entityDao;

  /** Instantiates a new Tile dao. */
  public TileDaoImpl() {
    mapper = ResultSetMapperFactory.getResultSetMapperIdentity();
    entityDao = new EntityDaoImpl();
    connection = new DatabaseConnection(TABLE_NAME);
  }

  /**
   * Sets connection.
   *
   * @param connection the connection
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setConnection(DatabaseConnection connection) {
    this.connection = connection;
  }

  /**
   * Sets mapper.
   *
   * @param mapper the mapper
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setMapper(ResultSetMapper mapper) {
    this.mapper = mapper;
  }

  /**
   * Sets entity dao.
   *
   * @param entityDao the entity dao
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setEntityDao(EntityDaoImpl entityDao) {
    this.entityDao = entityDao;
  }

  /**
   * Create.
   *
   * @param tileDto the tile
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void create(TileDto tileDto) {
    connection.connect();

    int costOfTraversal = tileDto instanceof ReachableTileDto ? ((ReachableTileDto) tileDto).getCostOfTraversal() : 0;

    DatabaseStatement statement = connection.createStatement("insert");
    statement.setString(1, tileDto.getTableName());
    statement.setInt(2, tileDto.getX());
    statement.setInt(3, tileDto.getY());
    statement.setInt(4, tileDto instanceof ReachableTileDto ? 1 : 0);
    statement.setInt(5, costOfTraversal);
    statement.execute();

    connection.close();

    if (tileDto instanceof ReachableTileDto) {
      for (EntityDto entityDto : ((ReachableTileDto) tileDto).getOccupyingEntities()) {
        entityDao.create(tileDto, entityDto);
      }
    }
  }

  /**
   * Read list.
   *
   * @param graphDto the world area
   * @return the list
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public List<TileDto> read(GraphDto graphDto) {
    List<TileDto> tileDtos = new ArrayList<>();
    connection.connect();
    DatabaseStatement statement = connection.createStatement("select");
    statement.setInt(1, graphDto.getStartX());
    statement.setInt(2, graphDto.getStartX() + graphDto.getSize());
    statement.setInt(3, graphDto.getStartY());
    statement.setInt(4, graphDto.getStartY() + graphDto.getSize());
    ResultSet result = statement.executeQuery();

    try {
      while (result.next()) {
        String name = result.getString("name");
        int x = result.getInt("x");
        int y = result.getInt("y");
        int costOfTraversal = result.getInt("costOfTraversal");
        tileDtos.add(TileMapper.map(name, x, y, costOfTraversal));
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
    connection.close();

    for (TileDto tileDto : tileDtos) {
      if (tileDto instanceof ReachableTileDto) {
        List<EntityDto> entities = entityDao.read(tileDto);
        ((ReachableTileDto) tileDto).setOccupyingEntities(entities);
      }
    }

    return tileDtos;
  }

  /**
   * Delete a tile.
   *
   * @param tile the tile to be deleted
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public void delete(TileDto tile) {
    if (tile instanceof ReachableTileDto) {
      for (EntityDto entity : ((ReachableTileDto) tile).getOccupyingEntities()) {
        entityDao.delete(tile.getX(), tile.getY(), entity);
      }
    }

    connection.connect();
    DatabaseStatement statement = connection.createStatement("delete");
    statement.setInt(1, tile.getX());
    statement.setInt(2, tile.getY());
    statement.execute();
    connection.close();
  }
}
