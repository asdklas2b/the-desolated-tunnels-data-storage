package nl.aimsites.nzbvuq.datastorage.dao;

import nl.aimsites.nzbvuq.datastorage.dto.graph.GraphDto;

import java.util.List;

/**
 * The interface World area dao.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public interface GraphDao {
  /**
   * Create.
   *
   * @param graphDto the graph
   */
  void create(GraphDto graphDto);

  /**
   * Read list.
   *
   * @return the list
   */
  List<GraphDto> read();

  /**
   * Delete a graph.
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void delete();
}
