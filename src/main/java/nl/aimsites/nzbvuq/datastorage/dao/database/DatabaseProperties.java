package nl.aimsites.nzbvuq.datastorage.dao.database;

import java.io.IOException;
import java.util.Objects;
import java.util.Properties;

/**
 * This class is responsible for the database properties, used for connecting to the SQLite
 * database.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class DatabaseProperties {
  private static final String PROPERTIES_FILE = "database.properties";
  private static final String DRIVER_PROPERTIES = "driver";
  private static final String CONNECTION_STRING_PROPERTIES = "connectionstring";
  private Properties properties;

  /** Instantiates a new Database properties. */
  public DatabaseProperties() {
    properties = new Properties();
    loadProperties(PROPERTIES_FILE);
  }

  /**
   * Sets properties.
   *
   * @param properties the properties
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setProperties(Properties properties) {
    this.properties = properties;
  }

  /**
   * Load properties.
   *
   * @param fileLocation the file location
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void loadProperties(String fileLocation) {
    try {
      properties.load(
          Objects.requireNonNull(getClass().getClassLoader().getResourceAsStream(fileLocation)));
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Gets driver.
   *
   * @return the driver
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public String getDriver() {
    return properties.getProperty(DRIVER_PROPERTIES);
  }

  /**
   * Gets connection string.
   *
   * @return the connection string
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public String getConnectionString() {
    return properties.getProperty(CONNECTION_STRING_PROPERTIES);
  }
}
