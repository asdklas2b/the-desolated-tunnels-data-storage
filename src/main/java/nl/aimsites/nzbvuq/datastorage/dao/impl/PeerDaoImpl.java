package nl.aimsites.nzbvuq.datastorage.dao.impl;

import nl.aimsites.nzbvuq.datastorage.dao.PeerDao;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;
import nl.jiankai.mapper.ResultSetMapper;
import nl.jiankai.mapper.ResultSetMapperFactory;

import java.sql.ResultSet;
import java.util.List;

/**
 * An implementation of the PeerDao interface
 *
 * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
 */
public class PeerDaoImpl implements PeerDao {
  private static final String TABLE_NAME = "players";
  protected DatabaseConnection connection;
  protected ResultSetMapper mapper;

  /** Instantiates a new Peer dao. */
  public PeerDaoImpl() {
    connection = new DatabaseConnection(TABLE_NAME);
    mapper = ResultSetMapperFactory.getResultSetMapperIdentity();
  }

  /**
   * Sets mapper.
   *
   * @param mapper the mapper
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setMapper(ResultSetMapper mapper) {
    this.mapper = mapper;
  }

  /**
   * Sets connection.
   *
   * @param connection the connection
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setConnection(DatabaseConnection connection) {
    this.connection = connection;
  }

  /**
   * Create.
   *
   * @param peerDto the peer
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public void create(PeerDto peerDto) {
    connection.connect();
    DatabaseStatement statement = connection.createStatement("insert");
    statement.setString(1, peerDto.getId());
    statement.executeQuery();
    connection.close();
  }

  /**
   * Read list.
   *
   * @return the list
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public List<PeerDto> read() {
    connection.connect();
    DatabaseStatement statement = connection.createStatement("select");
    ResultSet result = statement.executeQuery();
    List<PeerDto> peerDtos = mapper.map(result, PeerDto.class);
    connection.close();
    return peerDtos;
  }

  /**
   * Find peer.
   *
   * @param identifier the identifier
   * @return the peer
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public PeerDto find(Object identifier) {
    connection.connect();
    DatabaseStatement statement = connection.createStatement("select.id");
    statement.setString(1, (String) identifier);
    ResultSet result = statement.executeQuery();
    List<PeerDto> peerDtos = mapper.map(result, PeerDto.class);
    connection.close();
    return peerDtos.get(0);
  }

  /**
   * Update.
   *
   * @param identifier the identifier
   * @param peerDto the peer
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public void update(Object identifier, PeerDto peerDto) {
    connection.connect();
    DatabaseStatement statement = connection.createStatement("update");
    statement.setString(1, peerDto.getId());
    statement.setString(2, (String) identifier);
    statement.execute();
    connection.close();
  }

  /**
   * Delete.
   *
   * @param identifier the identifier
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public void delete(Object identifier) {
    connection.connect();
    DatabaseStatement statement = connection.createStatement("delete");
    statement.setString(1, (String) identifier);
    statement.execute();
    connection.close();
  }
}
