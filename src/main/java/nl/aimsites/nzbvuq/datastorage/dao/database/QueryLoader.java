package nl.aimsites.nzbvuq.datastorage.dao.database;

import java.io.IOException;
import java.util.Properties;

/**
 * This class is responsible for loading the (create, select, update etc) statements from
 * configuration files. Those configuration files are included within the resources folder.
 */
public class QueryLoader {
  private Properties properties;

  /** Instantiates a new Query loader. */
  public QueryLoader() {
    this.properties = new Properties();
  }

  /**
   * Set properties.
   *
   * @param properties the properties
   */
  public void setProperties(Properties properties) {
    this.properties = properties;
  }

  /**
   * Load query string string.
   *
   * @param tableName the table name
   * @param queryName the query name
   * @return the string
   */
  public String loadQueryString(String tableName, String queryName) {
    try {
      properties.load(
          getClass().getClassLoader().getResourceAsStream("queries/" + tableName + ".properties"));
    } catch (IOException e) {
      e.printStackTrace();
    }

    return properties.getProperty(queryName);
  }
}
