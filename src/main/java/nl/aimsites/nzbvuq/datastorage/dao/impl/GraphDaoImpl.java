package nl.aimsites.nzbvuq.datastorage.dao.impl;

import nl.aimsites.nzbvuq.datastorage.dao.GraphDao;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dto.graph.GraphDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;
import nl.jiankai.mapper.ResultSetMapper;
import nl.jiankai.mapper.ResultSetMapperFactory;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

/**
 * An implementation of the GraphDao interface
 *
 * @author Pieter van der Linden (P.vanderLinden@student.han.nl) & Stein Milder
 *     (SAJ.Milder@student.han.nl)
 */
public class GraphDaoImpl implements GraphDao {
  private static final String TABLE_NAME = "graphs";
  protected DatabaseConnection connection;
  protected ResultSetMapper mapper;
  private TileDaoImpl tileDao;
  private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

  /** Instantiates a new World area dao. */
  public GraphDaoImpl() {
    mapper = ResultSetMapperFactory.getResultSetMapperIdentity();
    connection = new DatabaseConnection(TABLE_NAME);
    tileDao = new TileDaoImpl();
  }

  /**
   * Sets mapper.
   *
   * @param mapper the mapper
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setMapper(ResultSetMapper mapper) {
    this.mapper = mapper;
  }

  /**
   * Sets connection.
   *
   * @param connection the connection
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setConnection(DatabaseConnection connection) {
    this.connection = connection;
  }

  /**
   * Sets tile dao.
   *
   * @param tileDao the tile dao
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setTileDao(TileDaoImpl tileDao) {
    this.tileDao = tileDao;
  }

  /**
   * Create.
   *
   * @param graphDto the graph
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  public void create(GraphDto graphDto) {
    connection.connect();

    DatabaseStatement statement = connection.createStatement("insert");
    statement.setInt(1, graphDto.getStartX());
    statement.setInt(2, graphDto.getStartY());
    statement.setInt(3, graphDto.getSize());
    statement.setLong(4, graphDto.getSeed());
    statement.execute();

    /*
     Filters the 2 dimensional array of tileDTOs for assigned tileDTOs, then saves the newly created tile in database
     using tileDao
    */
    Arrays.stream(graphDto.getTiles())
        .flatMap(Arrays::stream)
        .filter(Objects::nonNull)
        .forEach(tile -> tileDao.create(tile));

    connection.close();
  }

  /**
   * Read list.
   *
   * @return the list
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public List<GraphDto> read() {
    connection.connect();

    DatabaseStatement statement = connection.createStatement("select");
    ResultSet result = statement.executeQuery();
    List<GraphDto> graphDtos = mapper.map(result, GraphDto.class);

    connection.close();

    for (GraphDto graphDto : graphDtos) {
      graphDto.setTiles(new TileDto[graphDto.getSize()][graphDto.getSize()]);
      List<TileDto> tileDtos = tileDao.read(graphDto);

      for (TileDto tileDto : tileDtos) {
        graphDto.addTile(tileDto);
      }
    }

    return graphDtos;
  }

  /**
   * Delete.
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   *
   */
  public void delete() {
    List<String> tables = readStateTables();
    connection.connect();
    for (String table : tables) {
      DatabaseStatement statement = connection.createStatement("deleteall");
      statement.setTableName(table);
      statement.execute();
    }
    connection.close();
  }


  /**
   * Delete.
   * @return A list with the table names
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   *
   */
  private List<String> readStateTables() {
    connection.connect();
    DatabaseStatement statement = connection.createStatement("selectstatetables");
    ResultSet result = statement.executeQuery();
    List<String> tableNames = new ArrayList<>();
    try {
      while (result.next()) {
        tableNames.add(result.getString(2));
      }
    } catch (SQLException throwables) {
      LOGGER.warning("Error while reading readStateTables");
    }
    return tableNames;
  }
}
