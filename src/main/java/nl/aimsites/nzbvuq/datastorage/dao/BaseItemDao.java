package nl.aimsites.nzbvuq.datastorage.dao;

import nl.aimsites.nzbvuq.datastorage.dto.entities.items.BaseItemDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;

/**
 * This is an interface used for all the base items DAO's.
 *
 * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
 */
public interface BaseItemDao {
  /**
   * Create.
   *
   * @param tile the tile
   * @param baseItem the baseItem
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  void create(TileDto tile, BaseItemDto baseItem);

  BaseItemDto find(BaseItemDto baseItem, int x, int y);

  void delete(BaseItemDto baseItemDto, int x, int y);
}
