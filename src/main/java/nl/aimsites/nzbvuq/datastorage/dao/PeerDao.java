package nl.aimsites.nzbvuq.datastorage.dao;

import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;

import java.util.List;

/**
 * The interface Peer dao.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public interface PeerDao {
  /**
   * Create.
   *
   * @param peerDto the peer
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void create(PeerDto peerDto);

  /**
   * Read list.
   *
   * @return the list
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  List<PeerDto> read();

  /**
   * Find peer.
   *
   * @param identifier the identifier
   * @return the peer
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  PeerDto find(Object identifier);

  /**
   * Update.
   *
   * @param identifier the identifier
   * @param peerDto the peer
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void update(Object identifier, PeerDto peerDto);

  /**
   * Delete.
   *
   * @param identifier the identifier
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void delete(Object identifier);
}
