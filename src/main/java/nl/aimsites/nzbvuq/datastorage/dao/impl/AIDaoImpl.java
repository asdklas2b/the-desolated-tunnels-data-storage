package nl.aimsites.nzbvuq.datastorage.dao.impl;

import nl.aimsites.nzbvuq.datastorage.dao.AIDao;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.mapper.AIMapper;
import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * The type Ai dao.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class AIDaoImpl implements AIDao {
  private static final String TABLE_NAME = "ai";

  /** The Connection. */
  protected DatabaseConnection connection;

  /** The behaviourRuleDao, for creating and reading rules for an ai . */

  /**
   * Instantiates a new Ai dao.
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public AIDaoImpl() {
    connection = new DatabaseConnection(TABLE_NAME);
  }

  /**
   * Sets connection.
   *
   * @param connection the connection
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setConnection(DatabaseConnection connection) {
    this.connection = connection;
  }

  /**
   * Create an ai (monster/agent) with included behaviour rules
   *
   * @param aiDto the ai to be created
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public void create(AIDto aiDto) {
    connection.connect();

    DatabaseStatement statement = connection.createStatement("insert");
    statement.setString(1, aiDto.getName());
    statement.setString(2, aiDto.getType());
    statement.setString(3, aiDto.getCommand());
    statement.setString(4, aiDto.getPeer().getId());

    statement.execute();

    connection.close();
  }

  /**
   * Get a list of AI's (monsters/agents) from the database
   *
   * @return list of AI's
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public List<AIDto> read() {
    List<AIDto> aiDtos = new ArrayList<>();
    connection.connect();

    DatabaseStatement statement = connection.createStatement("select");
    ResultSet resultSet = statement.executeQuery();

    try {
      while (resultSet.next()) {
        AIDto aiDto = AIMapper.map(resultSet.getString("type"));
        aiDto.setName(resultSet.getString("name"));
        aiDto.setCommand(resultSet.getString("command"));
        PeerDto peerDto = new PeerDto();
        peerDto.setId(resultSet.getString("identifier"));
        aiDto.setPeer(peerDto);
        aiDtos.add(aiDto);
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }

    connection.close();

    return aiDtos;
  }

  /**
   * Delete an AI from the database
   *
   * @param aiDto the ai to be deleted
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public void delete(AIDto aiDto) {
    connection.connect();
    DatabaseStatement statement = connection.createStatement("delete");
    statement.setString(1, aiDto.getName());
    statement.setString(2, aiDto.getPeer().getId());
    statement.execute();
    connection.close();
  }
}
