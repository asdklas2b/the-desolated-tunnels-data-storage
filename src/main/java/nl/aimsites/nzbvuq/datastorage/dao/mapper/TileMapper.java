package nl.aimsites.nzbvuq.datastorage.dao.mapper;

import nl.aimsites.nzbvuq.datastorage.dto.graph.PointDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.*;

/**
 * Responsible for mapping data to a tile.
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class TileMapper {
  /**
   * Map tile.
   *
   * @param name the name
   * @param x the x
   * @param y the y
   * @param costOfTraversal the cost of traversal
   * @return the tile
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
public static TileDto map(String name, int x, int y, int costOfTraversal) {
    PointDto coordinates = new PointDto(x, y);

        return switch (name) {
            case "Room" -> new RoomDto(coordinates, costOfTraversal);
            case "Corridor" -> new CorridorDto(coordinates, costOfTraversal);
            case "Field" -> new FieldDto(coordinates, costOfTraversal);
            case "Forest" -> new ForestDto(coordinates);
            default -> null;
        };
    }
}
