package nl.aimsites.nzbvuq.datastorage.dao.impl;

import nl.aimsites.nzbvuq.datastorage.dao.BaseItemDao;
import nl.aimsites.nzbvuq.datastorage.dao.EntityDao;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dao.mapper.EntityMapper;
import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.BaseItemDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * An implementation of the EntityDao interface
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class EntityDaoImpl implements EntityDao {
  private static final String TABLE_NAME = "entity";
  private BaseItemDao baseItemDao;
  private DatabaseConnection connection;

  /** Instantiates a new Entity dao. */
  public EntityDaoImpl() {
    connection = new DatabaseConnection(TABLE_NAME);
    baseItemDao = new BaseItemDaoImpl();
  }

  /**
   * Sets connection.
   *
   * @param connection the connection
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setConnection(DatabaseConnection connection) {
    this.connection = connection;
  }

  public void setBaseItemDao(BaseItemDao baseItemDao) {
    this.baseItemDao = baseItemDao;
  }

  /**
   * Read list.
   *
   * @param tileDto the tile
   * @return the list
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public List<EntityDto> read(TileDto tileDto) {
    connection.connect();
    DatabaseStatement statement = connection.createStatement("select");
    statement.setInt(1, tileDto.getX());
    statement.setInt(2, tileDto.getY());
    ResultSet result = statement.executeQuery();

    List<EntityDto> entities = new ArrayList<>();

    try {
      while (result.next()) {
        String name = result.getString("name");
        int x = result.getInt("x");
        int y = result.getInt("y");

        EntityDto entityDto = EntityMapper.map(name);

        if (entityDto != null) {
          if (entityDto instanceof BaseItemDto) {
            entityDto = baseItemDao.find((BaseItemDto) entityDto, x, y);
          }
          entities.add(entityDto);
        }
      }
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }

    connection.close();

    return entities;
  }

  /**
   * Create.
   *
   * @param tileDto the tile
   * @param entityDto the entity
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void create(TileDto tileDto, EntityDto entityDto) {
    connection.connect();

    DatabaseStatement statement = connection.createStatement("insert");
    statement.setString(1, entityDto.getTableName());
    statement.setInt(2, tileDto.getX());
    statement.setInt(3, tileDto.getY());
    statement.execute();

    if (entityDto instanceof BaseItemDto) {
      baseItemDao.create(tileDto, (BaseItemDto) entityDto);
    }

    connection.close();
  }

  /**
   * Delete.
   *
   * @param x the x-coordinate of the entity to be deleted
   * @param y the y-coordinate of the entity to be deleted
   * @param entityDto the entity to be deleted
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public void delete(int x, int y, EntityDto entityDto) {
    if (entityDto instanceof BaseItemDto) {
      baseItemDao.delete((BaseItemDto) entityDto, x, y);
    }

    connection.connect();
    DatabaseStatement statement = connection.createStatement("delete");
    statement.setInt(1, x);
    statement.setInt(2, y);
    statement.execute();
    connection.close();
  }
}
