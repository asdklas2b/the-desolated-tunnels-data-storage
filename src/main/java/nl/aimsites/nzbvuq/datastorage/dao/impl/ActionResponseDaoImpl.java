package nl.aimsites.nzbvuq.datastorage.dao.impl;

import nl.aimsites.nzbvuq.datastorage.dao.ActionResponseDao;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseConnection;
import nl.aimsites.nzbvuq.datastorage.dao.database.DatabaseStatement;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;

/**
 * This is an implementation of the action response DAO's
 *
 * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
 */
public class ActionResponseDaoImpl implements ActionResponseDao {
  private static final String TABLE_NAME = "action_response";

  private DatabaseConnection connection;

  /**
   * This constructor is used to construct a action response implementation class
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public ActionResponseDaoImpl() {
    connection = new DatabaseConnection(TABLE_NAME);
  }

  public void setConnection(DatabaseConnection connection) {
    this.connection = connection;
  }

  /**
   * Create action response
   *
   * @param name  name of the entity
   * @param tile  the tile for the action response
   * @param value action response
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   */
  public void create(TileDto tile, String name, String value) {
    connection.connect();

    DatabaseStatement statement = connection.createStatement("insert");
    statement.setString(1, name);
    statement.setString(2, value);
    statement.setInt(3, tile.getX());
    statement.setInt(4, tile.getY());
    statement.execute();

    connection.close();
  }
}
