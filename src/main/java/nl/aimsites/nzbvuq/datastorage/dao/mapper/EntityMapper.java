package nl.aimsites.nzbvuq.datastorage.dao.mapper;

import nl.aimsites.nzbvuq.datastorage.dto.entities.ChestDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.ArmorDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.SpecialDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.StrengthDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.WeaponDto;
import nl.aimsites.nzbvuq.datastorage.dto.entities.items.special.FlagDto;

/**
 * This class is responsible for mapping database data to an entity, and backwards.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class EntityMapper {
  /**
   * Map entity.
   *
   * @return the entity
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
    public static EntityDto map(String name) {
        return switch (name) {
            case "Armor" -> new ArmorDto();
            case "Special" -> new SpecialDto();
            case "Strength" -> new StrengthDto();
            case "Weapon" -> new WeaponDto();
            case "Flag" -> new FlagDto();
            case "Chest" -> new ChestDto();
            default -> null;
        };
    }
}
