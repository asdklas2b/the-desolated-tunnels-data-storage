package nl.aimsites.nzbvuq.datastorage.dao.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * This class is responsible for the database connection.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class DatabaseConnection {
  private final String tableName;
  private Connection connection;

  /**
   * Instantiates a new Database connection.
   *
   * @param tableName the table name
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public DatabaseConnection(String tableName) {
    this.tableName = tableName;
  }

  /**
   * Gets connection.
   *
   * @return the connection
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public Connection getConnection() {
    return connection;
  }

  /**
   * Sets connection.
   *
   * @param connection the connection
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setConnection(Connection connection) {
    this.connection = connection;
  }

  /**
   * Create statement database statement.
   *
   * @param query the query
   * @return the database statement
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public DatabaseStatement createStatement(String query) {
    return new DatabaseStatement(connection, tableName, query);
  }

  /**
   * Connect.
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void connect() {
    try {
      String url = new DatabaseProperties().getConnectionString();
      connection = DriverManager.getConnection(url);
    } catch (SQLException e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Close.
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void close() {
    try {
      connection.close();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }
}
