package nl.aimsites.nzbvuq.datastorage.dao;

import nl.aimsites.nzbvuq.datastorage.dto.graph.GraphDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;

import java.util.List;

/**
 * The interface Tile dao.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public interface TileDao {
  /**
   * Create.
   *
   * @param tileDto the tile
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void create(TileDto tileDto);

  /**
   * Read list.
   *
   * @param graphDto the graph
   * @return the list
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  List<TileDto> read(GraphDto graphDto);

  /**
   * Delete a tile.
   *
   * @param tile the tile to be deleted
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void delete(TileDto tile);
}
