package nl.aimsites.nzbvuq.datastorage.dao;


import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;

/**
 * This is an interface used for action response DAO's
 *
 * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
 */
public interface ActionResponseDao {
    void create(TileDto tile, String name, String value);
}
