package nl.aimsites.nzbvuq.datastorage.dao.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * The type Database statement.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class DatabaseStatement {
  private final Connection connection;
  private final QueryLoader queryLoader;
  private final String tableName;
  private final String queryName;
  private PreparedStatement preparedStatement;

  /**
   * Instantiates a new Database statement.
   *
   * @param connection the connection
   * @param tableName the table name
   * @param queryName the query name
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public DatabaseStatement(Connection connection, String tableName, String queryName) {
    this(connection, tableName, queryName, new QueryLoader());
  }
  /**
   * Instantiates a new Database statement.
   *
   * @param connection the connection
   * @param tableName the table name
   * @param queryName the query name
   * @param queryLoader the query loader
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public DatabaseStatement(
      Connection connection, String tableName, String queryName, QueryLoader queryLoader) {
    this.connection = connection;
    this.tableName = tableName;
    this.queryName = queryName;
    this.queryLoader = queryLoader;

    String queryString = queryLoader.loadQueryString(tableName, queryName);
    prepare(queryString);
  }

  private void prepare(String queryString) {
    try {
      preparedStatement = connection.prepareStatement(queryString);
    } catch (SQLException throwables) {
      System.out.println("Query not ready");
    }
  }

  /**
   * Execute query result set.
   *
   * @return the result set
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public ResultSet executeQuery() {
    try {
      return preparedStatement.executeQuery();

    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }

    return null;
  }

  /**
   * Execute.
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void execute() {
    try {
      preparedStatement.execute();
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  public void setTableName(String name) {
    String queryString = queryLoader.loadQueryString(tableName, queryName);
    queryString = queryString.replace("$tableName", name);
    prepare(queryString);
  }

  /**
   * Sets string.
   *
   * @param index the index
   * @param value the value
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setString(int index, String value) {
    try {
      preparedStatement.setString(index, value);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  /**
   * Sets int.
   *
   * @param index the index
   * @param value the value
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setInt(int index, int value) {
    try {
      preparedStatement.setInt(index, value);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }

  /**
   * Sets long.
   *
   * @param index the index
   * @param value the value
   * @author Jos Elbers (J.Elbers2@student.han.nl)
   */
  public void setLong(int index, long value) {
    try {
      preparedStatement.setLong(index, value);
    } catch (SQLException throwables) {
      throwables.printStackTrace();
    }
  }
}
