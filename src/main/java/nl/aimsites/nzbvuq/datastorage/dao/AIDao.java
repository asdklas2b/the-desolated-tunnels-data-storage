package nl.aimsites.nzbvuq.datastorage.dao;

import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;

import java.util.List;

/**
 * The interface Ai dao.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public interface AIDao {
  /**
   * Create.
   *
   * @param aiDto the ai
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void create(AIDto aiDto);

  /**
   * Read list.
   *
   * @return the list
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  List<AIDto> read();

  /**
   * Delete.
   *
   * @param aiDto the ai
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void delete(AIDto aiDto);
}
