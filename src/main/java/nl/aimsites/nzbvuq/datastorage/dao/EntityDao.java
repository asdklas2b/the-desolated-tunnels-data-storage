package nl.aimsites.nzbvuq.datastorage.dao;

import nl.aimsites.nzbvuq.datastorage.dto.entities.EntityDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.tiles.TileDto;

import java.util.List;

/**
 * The interface Entity dao.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public interface EntityDao {
  /**
   * Read list.
   *
   * @param tileDto the tile
   * @return the list
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  List<EntityDto> read(TileDto tileDto);

  /**
   * Create.
   *
   * @param tileDto the tile
   * @param entityDto the entity
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void create(TileDto tileDto, EntityDto entityDto);

  /**
   * Delete.
   *
   * @param x the x-coordinate of the entity to be deleted
   * @param y the y-coordinate of the entity to be deleted
   * @param entityDto the entity to be deleted
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void delete(int x, int y, EntityDto entityDto);
}
