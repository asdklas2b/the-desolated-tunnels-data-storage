package nl.aimsites.nzbvuq.datastorage.crud;

import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;

import java.util.List;

/**
 * The interface Peers crud.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public interface PeersCrud {
  /**
   * Creates a new peer.
   *
   * @param peerDto The new peer to be created.
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void create(PeerDto peerDto);

  /**
   * Read all peers.
   *
   * @return the list
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  List<PeerDto> read();

  /**
   * Find a specific peer.
   *
   * @param identifier The new peer to be created.
   * @return the peer
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  PeerDto find(Object identifier);

  /**
   * Creates a new peer.
   *
   * @param identifier the identifier
   * @param peerDto The new peer to be created.
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void update(Object identifier, PeerDto peerDto);

  /**
   * Delete.
   *
   * @param identifier the identifier
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void delete(Object identifier);
}
