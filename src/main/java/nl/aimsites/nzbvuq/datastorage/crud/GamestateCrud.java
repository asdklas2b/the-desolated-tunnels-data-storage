package nl.aimsites.nzbvuq.datastorage.crud;

import nl.aimsites.nzbvuq.datastorage.dto.GamestateDto;

/**
 * The interface Gamestate crud.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public interface GamestateCrud {
  /**
   * Create.
   *
   * @param gamestateDto the gamestate
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void create(GamestateDto gamestateDto);

  /**
   * Read gamestate.
   *
   * @return the gamestate
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  GamestateDto read();

  /**
   * Update.
   *
   * @param gamestateDto the gamestate
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void update(GamestateDto gamestateDto);

  /**
   * Delete.
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void delete();
}
