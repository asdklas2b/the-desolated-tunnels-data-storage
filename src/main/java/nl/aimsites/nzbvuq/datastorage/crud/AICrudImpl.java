package nl.aimsites.nzbvuq.datastorage.crud;

import nl.aimsites.nzbvuq.datastorage.dao.AIDao;
import nl.aimsites.nzbvuq.datastorage.dao.impl.AIDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;

import java.util.List;

/**
 * An implementation of the AICrud interface
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class AICrudImpl implements AICrud {
  private AIDao aiDao;

  /**
   * Instantiates a new AI crud.
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public AICrudImpl() {
    aiDao = new AIDaoImpl();
  }

  public void setAiDao(AIDao aiDao) {
    this.aiDao = aiDao;
  }

  /**
   * @param aiDto - AI to be created
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public void create(AIDto aiDto) {
    aiDao.create(aiDto);
  }

  /**
   * @return - List of found AI's
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public List<AIDto> read() {
    return aiDao.read();
  }

  /**
   * @param aiDto - New values for AI
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public void update(AIDto aiDto) {
    aiDao.delete(aiDto);
    aiDao.create(aiDto);
  }

  /**
   * @param aiDto - The AI to be deleted
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public void delete(AIDto aiDto) {
    aiDao.delete(aiDto);
  }
}
