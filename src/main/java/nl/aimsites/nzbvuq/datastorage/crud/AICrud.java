package nl.aimsites.nzbvuq.datastorage.crud;

import nl.aimsites.nzbvuq.datastorage.dto.ai.AIDto;

import java.util.List;

/**
 * The interface Ai crud.
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public interface AICrud {
  /**
   * Create.
   *
   * @param aiDto the ai
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void create(AIDto aiDto);

  /**
   * Read list.
   *
   * @return the list
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  List<AIDto> read();

  /**
   * Update.
   *
   * @param aiDto the ai
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void update(AIDto aiDto);

  /**
   * Delete.
   *
   * @param aiDto the ai
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  void delete(AIDto aiDto);
}
