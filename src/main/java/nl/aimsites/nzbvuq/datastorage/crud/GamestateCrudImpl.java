package nl.aimsites.nzbvuq.datastorage.crud;

import nl.aimsites.nzbvuq.datastorage.dao.GraphDao;
import nl.aimsites.nzbvuq.datastorage.dao.impl.GraphDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.GamestateDto;
import nl.aimsites.nzbvuq.datastorage.dto.graph.GraphDto;

import java.util.List;

/**
 * An implementation of the GamestateCrud interface
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class GamestateCrudImpl implements GamestateCrud {
  private GraphDao graphDao;

  /**
   * Instantiates a new Gamestate crud.
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public GamestateCrudImpl() {
    this.graphDao = new GraphDaoImpl();
  }

  /**
   * Sets world area dao.
   *
   * @param graphDao the world area dao
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void setGraphDao(GraphDao graphDao) {
    this.graphDao = graphDao;
  }

  /**
   * @param gamestateDto - Game state that should be created
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public void create(GamestateDto gamestateDto) {
    List<GraphDto> graphs = gamestateDto.getGraphs();

    for (GraphDto graphDto : graphs) {
      graphDao.create(graphDto);
    }
  }

  /**
   * @return Found gamestate with graphs, tiles etc
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public GamestateDto read() {
    GamestateDto gamestateDto = new GamestateDto();
    List<GraphDto> graphDtos = graphDao.read();
    gamestateDto.setGraphs(graphDtos);
    return gamestateDto;
  }

  /**
   * @param gamestateDto - The new game state
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  @Override
  public void update(GamestateDto gamestateDto) {
    this.delete();
    this.create(gamestateDto);
  }

  /**
   * Delete all graphs in storage
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   */
  public void delete() {
    graphDao.delete();
  }
}
