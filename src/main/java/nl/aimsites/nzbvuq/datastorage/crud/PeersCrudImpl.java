package nl.aimsites.nzbvuq.datastorage.crud;

import nl.aimsites.nzbvuq.datastorage.dao.PeerDao;
import nl.aimsites.nzbvuq.datastorage.dao.impl.PeerDaoImpl;
import nl.aimsites.nzbvuq.datastorage.dto.PeerDto;

import java.util.List;

/**
 * An implementation of the PeersCrud interface
 *
 * @author Stein Milder (SAJ.Milder@student.han.nl)
 */
public class PeersCrudImpl implements PeersCrud {
  private PeerDao peerDao;

  /**
   * Instantiates a new Peers crud.
   *
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   * @author Vick Top (VA.Top@student.han.nl)
   */
  public PeersCrudImpl() {
    this.peerDao = new PeerDaoImpl();
  }

  /**
   * Sets peer dao.
   *
   * @param peerDao the peer dao
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   * @author Vick Top (VA.Top@student.han.nl)
   */
  public void setPeerDao(PeerDao peerDao) {
    this.peerDao = peerDao;
  }

  /**
   * Creates a new peer.
   *
   * @param peerDto - Peer that should be created
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   * @author Vick Top (VA.Top@student.han.nl)
   */
  @Override
  public void create(PeerDto peerDto) {
    peerDao.create(peerDto);
  }

  /**
   * Reads all peers.
   *
   * @return List of peers found
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   * @author Vick Top (VA.Top@student.han.nl)
   */
  @Override
  public List<PeerDto> read() {
    return peerDao.read();
  }

  /**
   * Finds specific peer.
   *
   * @return A specific peer found by identifier
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   * @author Vick Top (VA.Top@student.han.nl)
   */
  @Override
  public PeerDto find(Object identifier) {
    return peerDao.find(identifier);
  }

  /**
   * Updates a specific peer.
   *
   * @param identifier identifies the peer
   * @param peerDto contains the information the target peer will be updated with
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   * @author Vick Top (VA.Top@student.han.nl)
   */
  @Override
  public void update(Object identifier, PeerDto peerDto) {
    peerDao.update(identifier, peerDto);
  }

  /**
   * Deletes a specific peer.
   *
   * @param identifier identifies the peer
   * @author Stein Milder (SAJ.Milder@student.han.nl)
   * @author Pieter van der Linden (P.vanderLinden@student.han.nl)
   * @author Vick Top (VA.Top@student.han.nl)
   */
  @Override
  public void delete(Object identifier) {
    peerDao.delete(identifier);
  }
}
