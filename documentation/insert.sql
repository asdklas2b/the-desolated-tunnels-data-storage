/* BEGIN GAMESTATE */
BEGIN TRANSACTION;
INSERT INTO Graph (startX, startY, size, seed)
VALUES (20, 30, 50, 949494949);

INSERT INTO Tile (x, y, reachable, name, costOfTraversal)
VALUES (22, 33, 1, "Corridor", 20), (25, 35, 1, "Corridor", 20), (23, 35, 0, "Room", NULL);

INSERT INTO Entity (name, x, y)
VALUES('Weapon', 22, 33), ('Flag', 25, 35);

INSERT INTO Item (x, y, weight)
VALUES(22, 33, 5), (25, 35, 4);

INSERT INTO Weapon (x, y, usageCost, damage, maxStrengthAddition)
VALUES(22, 33, 50, 20, 60);

INSERT INTO Flag (x, y, teamNumber)
VALUES(25, 35, 5);
COMMIT;