-- Turn on use of foreign keys in SQLite
PRAGMA foreign_keys = ON;

/* BEGIN GAMESTATE */
CREATE TABLE Graph
(
    startX    INTEGER NOT NULL,
    startY    INTEGER NOT NULL,
    size      INTEGER NOT NULL,
    seed      BIGINT NOT NULL,

    PRIMARY KEY (startX, startY)
);

CREATE TABLE Tile
(
    name            VARCHAR(255) NOT NULL,

    x               INTEGER NOT NULL,
    y               INTEGER NOT NULL,
    reachable       TINYINT NOT NULL,
    costOfTraversal INTEGER NULL,

    PRIMARY KEY (x, y)
);

CREATE TABLE Entity
(
    name     VARCHAR(255) NOT NULL,

    x   INTEGER NOT NULL,
    y   INTEGER NOT NULL,

    FOREIGN KEY (x, y)
        REFERENCES Tile (x, y)
        ON DELETE CASCADE
);

CREATE TABLE Item
(
    weight   INTEGER NOT NULL,

    x   INTEGER NOT NULL,
    y   INTEGER NOT NULL,

  FOREIGN KEY (x, y)
    REFERENCES Entity (x, y)
    ON DELETE CASCADE
);

CREATE TABLE Action_Response
(
    name    VARCHAR(255) NOT NULL,
    value   VARCHAR(255) NOT NULL,

    x   INTEGER NOT NULL,
    y   INTEGER NOT NULL,

  FOREIGN KEY (x, y)
    REFERENCES Entity (x, y)
    ON DELETE CASCADE
);

CREATE TABLE Weapon
(
    usageCost INTEGER NOT NULL,
    damage    INTEGER NOT NULL,
    maxStrengthAddition INTEGER NOT NULL,

    x   INTEGER NOT NULL,
    y   INTEGER NOT NULL,

  FOREIGN KEY (x, y)
    REFERENCES Item (x, y)
    ON DELETE CASCADE
);

CREATE TABLE Strength
(
    strengthAddition INTEGER NOT NULL,

    x   INTEGER NOT NULL,
    y   INTEGER NOT NULL,

    FOREIGN KEY (x, y)
        REFERENCES Item (x, y)
        ON DELETE CASCADE
);

CREATE TABLE Special
(
    x   INTEGER NOT NULL,
    y   INTEGER NOT NULL,

    FOREIGN KEY (x, y)
        REFERENCES Item (x, y)
        ON DELETE CASCADE
);

CREATE TABLE Flag
(
    teamNumber INTEGER NOT NULL,

    x   INTEGER NOT NULL,
    y   INTEGER NOT NULL,

    FOREIGN KEY (x, y)
        REFERENCES Item (x, y)
        ON DELETE CASCADE
);

CREATE TABLE Armor
(
    strengthAddition INTEGER NOT NULL,
    maxStrengthAddition INTEGER NOT NULL,
    x   INTEGER NOT NULL,
    y   INTEGER NOT NULL,
    FOREIGN KEY (x, y)
        REFERENCES Item (x, y)
        ON DELETE CASCADE
);

/* END GAMESTATE */

CREATE TABLE AI
(
    name VARCHAR(255) NOT NULL,
    type VARCHAR(10) NOT NULL, -- Monster of Agent
    command TEXT NOT NULL,
    identifier VARCHAR(255) NOT NULL,

    PRIMARY KEY (identifier, name)
);










CREATE TABLE Team
(
    id INTEGER NOT NULL,

    PRIMARY KEY (id)
);

CREATE TABLE Character
(
    id      INTEGER NOT NULL,
    team_id INTEGER NOT NULL,

    PRIMARY KEY (id),
    FOREIGN KEY (team_id) REFERENCES Team (id)
);

CREATE TABLE Player
(
    entity_name VARCHAR(255) NOT NULL,

    FOREIGN KEY (entity_name) REFERENCES Entity (name)
);

CREATE TABLE Monster
(
    entity_name VARCHAR(255) NOT NULL,

    FOREIGN KEY (entity_name) REFERENCES Entity (name)
);

CREATE TABLE Agent
(
    entity_name VARCHAR(255) NOT NULL,

    FOREIGN KEY (entity_name) REFERENCES Entity (name)
);